package com.mit.virtualrunapp.registerActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.activityLogin.LoginActivity;
import com.mit.virtualrunapp.databinding.ActivityRegisterBinding;
import com.mit.virtualrunapp.helper.UserHelper;
import com.mit.virtualrunapp.utils.MFunction;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {


    ActivityRegisterBinding binding;
    ProgressDialog progressDialog;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;

    Button buttonRegister;
    EditText editTextUsername, editTextEmail, editTextPassword, editTextPhone, editTextAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        progressDialog = new ProgressDialog(this);
        setSupportActionBar(binding.toolbar);
        buttonRegister = findViewById(R.id.btnSubmit);


        buttonRegister.setOnClickListener(this::onClick);

        editTextUsername = findViewById(R.id.user_name);
        editTextEmail = findViewById(R.id.email);
        editTextPassword = findViewById(R.id.password);
        editTextPhone = findViewById(R.id.phone);
        editTextAddress  = findViewById(R.id.address);

    }

    private Boolean validate(){
        boolean valid = true;

        if(editTextUsername.getText().toString().isEmpty()){
            valid = false;
            editTextUsername.setError("Field cannot be empty");
        }

        if(editTextEmail.getText().toString().isEmpty()){
            valid = false;
            editTextEmail.setError("Field cannot be empty");
        }

        if(editTextPassword.getText().toString().isEmpty()){
            valid = false;
            editTextPassword.setError("Field cannot be empty");
        }

        if(editTextPhone.getText().toString().isEmpty()){
            valid = false;
            editTextPhone.setError("Field cannot be empty");
        }

        if(editTextAddress.getText().toString().isEmpty()){
            valid = false;
            editTextAddress.setError("Field cannot be empty.");
        }

        return valid;

    }

    @Override
    public void onClick(View v) {
        if(v == buttonRegister){
            if(MFunction.isInternetAvailable(this)){
                if(validate()) {
                    progressDialog.setMessage("Please wait... Creating User...");
                    showDialog();

                    buttonRegister.setClickable(false);

                    firebaseDatabase = FirebaseDatabase.getInstance();
                    reference = firebaseDatabase.getReference("users");

                    String userName = editTextUsername.getText().toString();
                    String email = editTextEmail.getText().toString();
                    String password = editTextPassword.getText().toString();
                    String phone = editTextPhone.getText().toString();
                    String address = editTextAddress.getText().toString();

                    UserHelper userHelper = new UserHelper(userName, email, password, phone, "normal","Active",address);
                    reference.child(userName).setValue(userHelper, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                            hideDialog();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                    });

                }

            } else {
                Toast.makeText(getApplicationContext(),"Please Connect to the internet",Toast.LENGTH_LONG).show();
            }



        }
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

}