package com.mit.virtualrunapp.database;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class UsersDB extends SugarRecord {

    private String email;
    private String mode;
    private String phone;
    private String userName;
    private String userStatus;
    private String userAddress;
    private String userPassword;


    public UsersDB(){

    }

    public UsersDB(String email, String  mode, String phone, String  userName, String userStatus, String userAddress,String userPassword){
        this.email = email;
        this.mode = mode;
        this.phone = phone;
        this.userName = userName;
        this.userStatus = userStatus;
        this.userAddress = userAddress;
        this.userPassword = userPassword;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public static List<UsersDB> getAllUsers(){
        List<UsersDB> usersDBS = Select.from(UsersDB.class).where(Condition.prop("mode").eq("normal")).list();
        if(usersDBS != null){
            return usersDBS;
        } else {
            return null;
        }
    }

}
