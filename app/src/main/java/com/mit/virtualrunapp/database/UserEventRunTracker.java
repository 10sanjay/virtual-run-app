package com.mit.virtualrunapp.database;

import com.mit.virtualrunapp.helper.UserRunTracker;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class UserEventRunTracker extends SugarRecord {
   private String latitude;
   private String longitude;
   private String distance;
   private String speed;
   private String eventName;
   private String userName;

    public UserEventRunTracker(){

    }

    public UserEventRunTracker(String latitude, String longitude, String distance, String speed, String eventName, String userName){
        this.latitude= latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.speed = speed;
        this.eventName = eventName;
        this.userName = userName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;

    }


    public static List<UserEventRunTracker> getrunListByusernameeventname(String userName, String eventName){
        List<UserEventRunTracker> userEventRunTrackers = Select.from(UserEventRunTracker.class).where(Condition.prop("user_name").eq(userName)).where(Condition.prop("event_name").eq(eventName)).list();
        if(userEventRunTrackers.isEmpty()){
            return null;
        } else {
            return userEventRunTrackers;
        }
    }

    public static String getLatitudeByUserName(String userName){
        List<UserEventRunTracker> userEventRunTrackers = Select.from(UserEventRunTracker.class).where(Condition.prop("user_name").eq(userName)).list();

        if(userEventRunTrackers.isEmpty()){
            return "";
        } else {
            return userEventRunTrackers.get(userEventRunTrackers.size()-1).getLatitude();
        }

    }

    public static String getLongitudeByUserName(String userName){
        List<UserEventRunTracker> userEventRunTrackers = Select.from(UserEventRunTracker.class).where(Condition.prop("user_name").eq(userName)).list();

        if(userEventRunTrackers.isEmpty()){
            return "";
        } else {
            return userEventRunTrackers.get(userEventRunTrackers.size()-1).getLongitude();
        }

    }


}
