package com.mit.virtualrunapp.database;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class FinalScoreDB extends SugarRecord {

    private String userName;
    private String eventName;
    private float totalDistance;
    private float avgSpeed;

    public FinalScoreDB(){

    }

    public FinalScoreDB(String userName, String eventName, float totalDistance, float avgSpeed){
        this.userName = userName;
        this.eventName = eventName;
        this.totalDistance = totalDistance;
        this.avgSpeed = avgSpeed;
        save();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public float getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(float totalDistance) {
        this.totalDistance = totalDistance;
    }

    public float getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(float avgSpeed) {
        this.avgSpeed = avgSpeed;
    }


    public static List<FinalScoreDB> getFinalScoreBySpeed(String eventName){
        String query = "SELECT * FROM FINAL_SCORE_DB WHERE event_name = '"+eventName+"' ORDER BY avg_speed DESC";
        List<FinalScoreDB> finalScoreDBS = FinalScoreDB.findWithQuery(FinalScoreDB.class,query);
        if(finalScoreDBS.isEmpty()){
            return null;
        } else {
            return finalScoreDBS;
        }

    }

}
