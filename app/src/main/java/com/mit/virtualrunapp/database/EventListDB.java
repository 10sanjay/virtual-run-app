package com.mit.virtualrunapp.database;

import com.mit.virtualrunapp.utils.MFunction;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class EventListDB extends SugarRecord {
    private String name;
    private String startDate;
    private String endDate;
    private String startPoint;
    private String endPoint;
    private String startTime;
    private String status;
    private Long unixStartDate;
    private Long unixEndDate;

    public EventListDB() {

    }

    public EventListDB(String name, String startDate, String endDate, String startPoint, String endPoint, String startTime, String status) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.startTime = startTime;
        this.status = status;
        this.unixStartDate = MFunction.getUnixTimestamp(startDate, "yyyy-MM-dd");
        this.unixEndDate = MFunction.getUnixTimestamp(endDate,"yyyy-MM-dd");

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static List<EventListDB> getAllEventList() {
        List<EventListDB> eventListDBS = Select.from(EventListDB.class).list();
        if (eventListDBS != null) {
            return eventListDBS;
        } else {
            return null;
        }
    }

    public static List<EventListDB> getStartedEvents(Long date) {
        String query = "SELECT * FROM EVENT_LIST_DB WHERE unix_start_date <= " + date + " AND unix_end_date >=" + date;
        List<EventListDB> eventListDBS = EventListDB.findWithQuery(EventListDB.class, query);
        if (eventListDBS.isEmpty()) return null;
        return eventListDBS;
    }

    public static List<EventListDB> getFinishedEvents(Long date) {
        String query = "SELECT * FROM EVENT_LIST_DB WHERE unix_start_date <=" + date;
        List<EventListDB> eventListDBS = EventListDB.findWithQuery(EventListDB.class, query);
        if (eventListDBS.isEmpty()) return null;
        return eventListDBS;
    }

    public static String getStartPoint(String eventname){
        List<EventListDB> eventListDBS = Select.from(EventListDB.class).where(Condition.prop("name").eq(eventname)).list();
        return eventListDBS.get(0).startPoint;
    }

    public static String getEndPoint(String eventname){
        List<EventListDB> eventListDBS = Select.from(EventListDB.class).where(Condition.prop("name").eq(eventname)).list();
        return eventListDBS.get(0).endPoint;
    }

}
