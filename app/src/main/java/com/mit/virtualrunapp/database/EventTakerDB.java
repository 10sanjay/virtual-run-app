package com.mit.virtualrunapp.database;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class EventTakerDB extends SugarRecord {

    public String userName;
    public String eventName;
    public String status;

    public EventTakerDB(){
    }

    public EventTakerDB(String userName, String eventName, String status){
        this.userName= userName;
        this.eventName = eventName;
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static List<EventTakerDB> getEventTakerByEventName(String eventName){
        List<EventTakerDB> eventTakerDBS = Select.from(EventTakerDB.class).where(Condition.prop("event_name").eq(eventName)).where(Condition.prop("status").eq("Take Part")).list();
        if(eventTakerDBS != null){
            return eventTakerDBS;
        } else {
            return null;
        }
    }

    public static String getStatusByUnameNEname(String eventName, String userName){
        List<EventTakerDB> eventTakerDBS = Select.from(EventTakerDB.class).where(Condition.prop("event_name").eq(eventName)).where(Condition.prop("user_name").eq(userName)).list();

        if(!eventTakerDBS.isEmpty()){
            return eventTakerDBS.get(0).status;
        } else {
            return "";
        }

    }

}
