package com.mit.virtualrunapp.activityLogin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.admindashboard.MainActivity;
import com.mit.virtualrunapp.databinding.ActivityLoginBinding;
import com.mit.virtualrunapp.registerActivity.RegisterActivity;
import com.mit.virtualrunapp.userdashboard.IndexActivity;
import com.mit.virtualrunapp.utils.MFunction;
import com.mit.virtualrunapp.utils.UserSessionManager;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;

    DatabaseReference reference;

    UserSessionManager userSessionManager;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        userSessionManager = new UserSessionManager(getApplicationContext());

        progressDialog = new ProgressDialog(this);


        binding.loginSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                loginUser();


            }
        });

        binding.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });


    }

    private Boolean validateUserName() {
        String val = binding.userName.getText().toString();

        if (val.isEmpty()) {
            binding.userName.setError("Field cannot be empty.");
            return false;
        } else {
            return true;
        }
    }

    private Boolean validatePassword() {
        String vals = binding.password.getText().toString();

        if (vals.isEmpty()) {
            binding.password.setError("Field cannot be empty.");
            return false;
        } else {
            return true;
        }
    }

    public void loginUser() {
        if (!validateUserName() | !validatePassword()) {
            return;
        } else {
            if(MFunction.isInternetAvailable(getApplicationContext())){
                isUser();

            } else {
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void isUser() {
        progressDialog.setMessage("Please wait");
        showDialog();
        String userEnteredUserName = binding.userName.getText().toString().trim();
        String userEnteredPassword = binding.password.getText().toString().trim();

        reference = FirebaseDatabase.getInstance().getReference("users");

        Query checkUser = reference.orderByChild("username").equalTo(userEnteredUserName);
        checkUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if (snapshot.exists()) {
                    String passwordFromDB = snapshot.child(userEnteredUserName).child("password").getValue(String.class);

                    if (passwordFromDB.equals(userEnteredPassword)) {
                        String userNameDB = snapshot.child(userEnteredUserName).child("username").getValue(String.class);
                        String emailDB = snapshot.child(userEnteredUserName).child("email").getValue(String.class);
                        String phoneDB = snapshot.child(userEnteredUserName).child("phone").getValue(String.class);
                        String modeDB = snapshot.child(userEnteredUserName).child("mode").getValue(String.class);
                        String addressDB = snapshot.child(userEnteredUserName).child("address").getValue(String.class);
                        String statusDB = snapshot.child(userEnteredUserName).child("status").getValue(String.class);

                        if(statusDB.equals("InActive")){
                            hideDialog();
                            Toast.makeText(getApplicationContext(),"Your account is disabled. Please contact admin",Toast.LENGTH_LONG).show();
                        } else {
                            Intent intent;
                            if(modeDB.equals("admin")){
                                intent = new Intent(getApplicationContext(), MainActivity.class);
                            } else {
                                intent = new Intent(getApplicationContext(), IndexActivity.class);
                            }
                            startActivity(intent);

                            userSessionManager.setLogin(true);
                            userSessionManager.setUserMode(modeDB);
                            userSessionManager.setUserName(userNameDB);
                            userSessionManager.setUserPhone(phoneDB);
                            userSessionManager.setUserAddress(addressDB);
                            userSessionManager.setUserEmail(emailDB);
                            hideDialog();
                        }
                    } else {
                        binding.password.setError("Wrong Password.");
                        hideDialog();
                    }

                } else {
                    binding.userName.setError("Username does not exists.");
                    hideDialog();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

}