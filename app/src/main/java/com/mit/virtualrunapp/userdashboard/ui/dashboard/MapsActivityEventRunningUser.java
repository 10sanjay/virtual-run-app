package com.mit.virtualrunapp.userdashboard.ui.dashboard;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.database.UserEventRunTracker;
import com.mit.virtualrunapp.databinding.ActivityMapsEventRunningUserBinding;
import com.mit.virtualrunapp.helper.UserRunTracker;
import com.mit.virtualrunapp.utils.UserSessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class MapsActivityEventRunningUser extends FragmentActivity implements OnMapReadyCallback {

    List<UserEventRunTracker> userEventRunTrackers;
    String eventNAME;
    DatabaseReference reference;
    Polyline polyline = null;
    GoogleMap mMap;

    private ActivityMapsEventRunningUserBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsEventRunningUserBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        eventNAME = getIntent().getStringExtra("eventname");

        reference = FirebaseDatabase.getInstance().getReference("eventruntracker");


        reference.child(eventNAME+""+new UserSessionManager(getApplicationContext()).getUserName()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                UserEventRunTracker.deleteAll(UserEventRunTracker.class);

                for (DataSnapshot dataSnapshot :  snapshot.getChildren()){

                    UserRunTracker userRunTracker = dataSnapshot.getValue(UserRunTracker.class);
                    UserEventRunTracker userEventRunTracker = new UserEventRunTracker(userRunTracker.getLatitude(),userRunTracker.getLongitude(),userRunTracker.getDistance(),userRunTracker.getSpeed(),userRunTracker.getEventname(),userRunTracker.getUsername());
                    userEventRunTracker.save();

                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        String[] intPoint = EventListDB.getStartPoint(eventNAME).split(",");
        String[] finalPoint = EventListDB.getEndPoint(eventNAME).split(",");
        LatLng startPoint = new LatLng(Double.parseDouble(intPoint[0]),Double.parseDouble(intPoint[1]));
        LatLng endPoint = new LatLng(Double.parseDouble(finalPoint[0]),Double.parseDouble(finalPoint[1]));
        LatLng sydney = new LatLng(Double.parseDouble(new UserSessionManager(getApplicationContext()).getPreviousLat()), Double.parseDouble(new UserSessionManager(getApplicationContext()).getPreviousLong()));
        mMap.addMarker(new MarkerOptions().position(sydney).title(new UserSessionManager(getApplicationContext()).getUserName()));
        mMap.addMarker(new MarkerOptions().position(startPoint).title("Start Line").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_blue)));
        mMap.addMarker(new MarkerOptions().position(endPoint).title("Finish Line").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_green)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setPath();

            }
        }, 3000);
    }

    public void setPath(){
        userEventRunTrackers = UserEventRunTracker.getrunListByusernameeventname(new UserSessionManager(getApplicationContext()).getUserName(),eventNAME);

        List<LatLng> latLngList = new ArrayList<>();
        if(userEventRunTrackers != null){
            for (int i =0; i < userEventRunTrackers.size();i++){
                LatLng latLng = new LatLng(Double.parseDouble(userEventRunTrackers.get(i).getLatitude()),Double.parseDouble(userEventRunTrackers.get(i).getLongitude()));
                latLngList.add(latLng);
            }
        }

        PolylineOptions polylineOptions = new PolylineOptions().addAll(latLngList).width(15).color(Color.RED);
        polyline = mMap.addPolyline(polylineOptions);
    }
}