package com.mit.virtualrunapp.userdashboard.ui.dashboard;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.admindashboard.MainActivity;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.database.EventTakerDB;
import com.mit.virtualrunapp.helper.EventTakerHelper;
import com.mit.virtualrunapp.services.LocationUpdatesService;
import com.mit.virtualrunapp.userdashboard.ui.home.HomeEventListAdapter;
import com.mit.virtualrunapp.utils.MFunction;
import com.mit.virtualrunapp.utils.UserSessionManager;

import java.util.List;

public class DashboardEventListAdapter extends RecyclerView.Adapter<DashboardEventListAdapter.ViewHolder> {

    Activity activity;
    List<EventListDB> eventListDBS;
    private boolean isFragment = false;
    private LocationUpdatesService mService = null;
    private static final int LOCATION_PERMISSIONS_REQUEST_CODE = 34;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;
    boolean hasLocationPermission = false;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        private boolean mBound = false;


        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            if(new UserSessionManager(activity).isUserRunning()) {

                startTrackerService();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    public DashboardEventListAdapter(Activity activity, List<EventListDB> eventListDBS, Boolean isFragment) {
        this.activity = activity;
        this.eventListDBS = eventListDBS;
        this.isFragment = isFragment;
    }

    @Override
    public DashboardEventListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_eventstart_client, viewGroup, false);
        DashboardEventListAdapter.ViewHolder mh = new DashboardEventListAdapter.ViewHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(DashboardEventListAdapter.ViewHolder holder, int position) {
        EventListDB details = eventListDBS.get(position);

        if(new UserSessionManager(activity).getUserRunningEvent()!=null){
        if(new UserSessionManager(activity).getUserRunningEvent().equals(details.getName())){
            holder.textViewTakeInOut.setText("Stop Run");


        }}

        holder.textViewName.setText(details.getName());
        holder.textViewStartDate.setText(details.getStartDate());
        holder.textViewEndDate.setText(details.getEndDate());


        holder.holderRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new UserSessionManager(activity).isUserRunning()){
                    if(new UserSessionManager(activity).getUserRunningEvent().equals(details.getName())){
                    Intent intent = new Intent(activity,MapsActivityEventRunningUser.class);
                    intent.putExtra("eventname",details.getName());
                    activity.startActivity(intent);

                    } else {
                        Toast.makeText(activity,"You are not running in this event.",Toast.LENGTH_LONG).show();
                    }
                }

            }
        });





        holder.textViewTakeInOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MFunction.isInternetAvailable(activity)){
                    hasLocationPermission = MFunction.hasPermissions(activity, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);

                    if(hasLocationPermission){
                        if(new UserSessionManager(activity).isUserRunning()){

                            if(holder.textViewTakeInOut.getText().equals("Stop Run")){

                                stopTrackerService();
                                holder.textViewTakeInOut.setText("Start Run");
                                holder.textViewTakeInOut.setBackground(activity.getResources().getDrawable(R.drawable.button_greem_bg));

                                new UserSessionManager(activity).setUserRunningEvent(null);
                                new UserSessionManager(activity).setUserRunning(false);
                            } else {

                                if(holder.textViewTakeInOut.getText().equals("Start Run")){
                                    Toast.makeText(activity,"Please end previous run",Toast.LENGTH_LONG).show();
                                } else {

                                    startTrackerService();
                                    holder.textViewTakeInOut.setBackground(activity.getResources().getDrawable(R.drawable.button_yellow_bg));
                                    holder.textViewTakeInOut.setText("Stop Run");
                                    new UserSessionManager(activity).setUserRunningEvent(details.getName());
                                    new UserSessionManager(activity).setUserRunning(true);
                                }
                            }
                        } else {

                            if(EventTakerDB.getStatusByUnameNEname(details.getName(),new UserSessionManager(activity.getApplicationContext()).getUserName()).equals("Take Part")){
                                startTrackerService();
                                holder.textViewTakeInOut.setText("Stop Run");
                                holder.textViewTakeInOut.setBackground(activity.getResources().getDrawable(R.drawable.button_yellow_bg));
                                new UserSessionManager(activity).setUserRunningEvent(details.getName());
                                new UserSessionManager(activity).setUserRunning(true);
                            } else {
                                Toast.makeText(activity.getApplicationContext(), "You have not taken part in this event",Toast.LENGTH_LONG).show();
                            }


                        }
                    } else {
                        requestPermissions();
                    }




                } else {
                    Toast.makeText(activity,"Please connect to internet.",Toast.LENGTH_LONG).show();
                }

            }
        });

        activity.bindService(new Intent(activity, LocationUpdatesService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);

    }

    @Override
    public int getItemCount() {
        return eventListDBS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName, textViewStartDate, textViewEndDate;
        Button textViewTakeInOut;
        LinearLayout layout;
        RelativeLayout holderRelativeLayout;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = view.findViewById(R.id.tvName);
            this.textViewStartDate = view.findViewById(R.id.tvStartDate);
            this.textViewEndDate = view.findViewById(R.id.tvEndDate);
            this.textViewTakeInOut = view.findViewById(R.id.tvInOut);
            this.layout = view.findViewById(R.id.holderInOut);
            this.holderRelativeLayout = view.findViewById(R.id.holderEventStart);
        }
    }

    private void startTrackerService() {


            if (mService != null) {
                mService.requestLocationUpdates();
            }


    }

    private void stopTrackerService() {
        if (mService != null) {
            mService.removeLocationUpdates();
            new UserSessionManager(activity).setUserPreviouseLat(null);
            new UserSessionManager(activity).setUserPreviouseLong(null);
            new UserSessionManager(activity).setUserTimeLastLocationTrack(null);
        }
    }


    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i("request permission", "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    activity.findViewById(android.R.id.content),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION ,Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                                    LOCATION_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i("Requesting Permission", "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION ,Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                    LOCATION_PERMISSIONS_REQUEST_CODE);
        }
    }

}
