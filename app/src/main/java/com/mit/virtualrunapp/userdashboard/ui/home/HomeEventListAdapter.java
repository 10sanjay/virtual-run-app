package com.mit.virtualrunapp.userdashboard.ui.home;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.admindashboard.EventListAdapter;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.database.EventTakerDB;
import com.mit.virtualrunapp.helper.EventTakerHelper;
import com.mit.virtualrunapp.utils.MFunction;
import com.mit.virtualrunapp.utils.UserSessionManager;

import java.util.List;


public class HomeEventListAdapter extends RecyclerView.Adapter<HomeEventListAdapter.ViewHolder> {

    Activity activity;
    List<EventListDB> eventListDBS;
    private boolean isFragment = false;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;

    public HomeEventListAdapter(Activity activity, List<EventListDB> eventListDBS, Boolean isFragment) {
        this.activity = activity;
        this.eventListDBS = eventListDBS;
        this.isFragment = isFragment;
    }

    @Override
    public HomeEventListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_eventlist_client, viewGroup, false);
        HomeEventListAdapter.ViewHolder mh = new HomeEventListAdapter.ViewHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(HomeEventListAdapter.ViewHolder holder, int position) {
        EventListDB details = eventListDBS.get(position);
        holder.textViewName.setText(details.getName());
        holder.textViewStartDate.setText(details.getStartDate());
        holder.textViewEndDate.setText(details.getEndDate());

        if(!EventTakerDB.getStatusByUnameNEname(details.getName(),new UserSessionManager(activity).getUserName()).isEmpty()){
            if(EventTakerDB.getStatusByUnameNEname(details.getName(),new UserSessionManager(activity).getUserName()).equals("Take Part")){
                holder.textViewTakeInOut.setText("Part Out");
                holder.textViewTakeInOut.setBackground(activity.getResources().getDrawable(R.drawable.button_yellow_bg));

            } else {
                holder.textViewTakeInOut.setText("Take Part");
                holder.textViewTakeInOut.setBackground(activity.getResources().getDrawable(R.drawable.button_greem_bg));

            }
        }


        holder.textViewTakeInOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MFunction.isInternetAvailable(activity)){

                    firebaseDatabase = FirebaseDatabase.getInstance();
                    reference = firebaseDatabase.getReference("eventtaker");


                    if(holder.textViewTakeInOut.getText().equals("Take Part")){
                        holder.textViewTakeInOut.setBackground(activity.getResources().getDrawable(R.drawable.button_yellow_bg));

                        holder.textViewTakeInOut.setText("Part Out");

                        EventTakerHelper eventTakerHelper = new EventTakerHelper(details.getName(),"Take Part",new UserSessionManager(activity.getApplicationContext()).getUserName());

                        reference.child(details.getName()+""+new UserSessionManager(activity.getApplicationContext()).getUserName()).setValue(eventTakerHelper, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                                Toast.makeText(activity.getApplicationContext(), "Successfully Taken part.", Toast.LENGTH_LONG).show();
                            }
                        });

                    } else {
                        holder.textViewTakeInOut.setBackground(activity.getResources().getDrawable(R.drawable.button_greem_bg));
                        holder.textViewTakeInOut.setText(R.string.takepart);

                        EventTakerHelper eventTakerHelper = new EventTakerHelper(details.getName(),"Part Out",new UserSessionManager(activity.getApplicationContext()).getUserName());

                        reference.child(details.getName()+""+new UserSessionManager(activity.getApplicationContext()).getUserName()).setValue(eventTakerHelper, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                                Toast.makeText(activity.getApplicationContext(), "Successfully Out from Taken part.", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    Toast.makeText(activity,"Please connect to internet.",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return eventListDBS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName, textViewStartDate, textViewEndDate;
        Button textViewTakeInOut;
        LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = view.findViewById(R.id.tvName);
            this.textViewStartDate = view.findViewById(R.id.tvStartDate);
            this.textViewEndDate = view.findViewById(R.id.tvEndDate);
            this.textViewTakeInOut = view.findViewById(R.id.tvInOut);
            this.layout = view.findViewById(R.id.holderInOut);
        }
    }


}
