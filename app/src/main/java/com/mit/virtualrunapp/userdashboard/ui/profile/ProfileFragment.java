package com.mit.virtualrunapp.userdashboard.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.mit.virtualrunapp.activityLogin.LoginActivity;
import com.mit.virtualrunapp.admindashboard.ActivityFinalResult;
import com.mit.virtualrunapp.databinding.FragmentNotificationsBinding;
import com.mit.virtualrunapp.utils.UserSessionManager;


public class ProfileFragment extends Fragment {

    UserSessionManager userSessionManager;
    private ProfileViewModel profileViewModel;
    private FragmentNotificationsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);
        userSessionManager = new UserSessionManager(getContext());

        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        binding.tvAddress.setText(new UserSessionManager(getContext()).getUserAddress());
        binding.tvEmail.setText(new UserSessionManager(getContext()).getUserEmail());
        binding.tvPhone.setText(new UserSessionManager(getContext()).getUserPhone());



        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSessionManager.setLogin(false);
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                getActivity().finishAffinity();

            }
        });

        binding.viewResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ActivityFinalResult.class);
                startActivity(intent);
            }
        });




        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}