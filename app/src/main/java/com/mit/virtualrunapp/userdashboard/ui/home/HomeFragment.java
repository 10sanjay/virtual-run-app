package com.mit.virtualrunapp.userdashboard.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.admindashboard.EventListAdapter;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.database.EventTakerDB;
import com.mit.virtualrunapp.databinding.FragmentHomeBinding;
import com.mit.virtualrunapp.helper.EventTakerHelper;
import com.mit.virtualrunapp.helper.NewEventHelper;
import com.orm.SugarContext;
import com.orm.SugarRecord;

import java.util.List;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference,referenceET;
    RecyclerView recyclerView;
    HomeEventListAdapter homeEventListAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        SugarContext.init(this.getContext());

        referenceET = FirebaseDatabase.getInstance().getReference("eventtaker");

        referenceET.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                EventTakerDB.deleteAll(EventTakerDB.class);

                for (DataSnapshot postnap : snapshot.getChildren()) {
                    EventTakerHelper eventTakerHelper = postnap.getValue(EventTakerHelper.class);
                    EventTakerDB eventListDB = new EventTakerDB(eventTakerHelper.getUsername(), eventTakerHelper.getEventname(), eventTakerHelper.getStatus());
                    eventListDB.save();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        reference = FirebaseDatabase.getInstance().getReference("events");
        recyclerView = binding.rvEventList;
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                EventListDB.deleteAll(EventListDB.class);

                for (DataSnapshot postnap : snapshot.getChildren()) {
                    NewEventHelper eventHelper = postnap.getValue(NewEventHelper.class);
                    EventListDB eventListDB = new EventListDB(eventHelper.getEventName(), eventHelper.getStartDate(), eventHelper.getEndDate(), eventHelper.getStartPoint(), eventHelper.getEndPoint(), eventHelper.getStartTime(), eventHelper.getStatus());
                    eventListDB.save();
                }

                List<EventListDB> eventListDBS;
                eventListDBS = EventListDB.getAllEventList();
                setRvEventList(eventListDBS);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        return root;
    }

    private void setRvEventList(List<EventListDB> eventListDBS) {
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        homeEventListAdapter = new HomeEventListAdapter(getActivity(), eventListDBS, true);
        recyclerView.setAdapter(homeEventListAdapter);
        homeEventListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}