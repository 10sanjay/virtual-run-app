package com.mit.virtualrunapp.userdashboard.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.databinding.FragmentDashboardBinding;
import com.mit.virtualrunapp.helper.NewEventHelper;
import com.mit.virtualrunapp.userdashboard.ui.home.HomeEventListAdapter;
import com.mit.virtualrunapp.utils.MFunction;
import com.orm.SugarContext;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private FragmentDashboardBinding binding;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;
    RecyclerView recyclerView;

    DashboardEventListAdapter dashboardEventListAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);

        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        SugarContext.init(this.getContext());

        reference = FirebaseDatabase.getInstance().getReference("events");
        recyclerView = binding.rvEventList;
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                EventListDB.deleteAll(EventListDB.class);

                for (DataSnapshot postnap : snapshot.getChildren()) {
                    NewEventHelper eventHelper = postnap.getValue(NewEventHelper.class);
                    EventListDB eventListDB = new EventListDB(eventHelper.getEventName(), eventHelper.getStartDate(), eventHelper.getEndDate(), eventHelper.getStartPoint(), eventHelper.getEndPoint(), eventHelper.getStartTime(), eventHelper.getStatus());
                    eventListDB.save();
                }

                Date c = Calendar.getInstance().getTime();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                String formattedDate = df.format(c);

                List<EventListDB> eventListDBS;
                eventListDBS = EventListDB.getStartedEvents(MFunction.getUnixTimestamp(formattedDate));
                setRvEventList(eventListDBS);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        return root;
    }

    private void setRvEventList(List<EventListDB> eventListDBS) {
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        dashboardEventListAdapter = new DashboardEventListAdapter(getActivity(), eventListDBS, true);
        recyclerView.setAdapter(dashboardEventListAdapter);
        dashboardEventListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}