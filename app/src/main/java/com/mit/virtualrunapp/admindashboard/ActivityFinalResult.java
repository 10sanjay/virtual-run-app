package com.mit.virtualrunapp.admindashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.utils.MFunction;
import com.orm.SugarContext;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ActivityFinalResult extends AppCompatActivity {
    RecyclerView recyclerView;
    FinalResultAdapter finalResultAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_result);
        setTitle("Final Score");
        SugarContext.init(this);

        recyclerView = findViewById(R.id.rvEventList);

        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);

        List<EventListDB> eventListDBS;
        eventListDBS = EventListDB.getFinishedEvents(MFunction.getUnixTimestamp(formattedDate));
        if(eventListDBS != null){
            setRvEventList(eventListDBS);
        } else {
            Toast.makeText(getApplicationContext(),"There is no any finished event. Come back later",Toast.LENGTH_LONG).show();
        }
    }

    private void setRvEventList(List<EventListDB> eventListDBS) {
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        finalResultAdapter = new FinalResultAdapter(this, eventListDBS, false);
        recyclerView.setAdapter(finalResultAdapter);
        finalResultAdapter.notifyDataSetChanged();
    }
}