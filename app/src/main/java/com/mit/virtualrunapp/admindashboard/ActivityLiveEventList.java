package com.mit.virtualrunapp.admindashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.userdashboard.ui.dashboard.DashboardEventListAdapter;
import com.mit.virtualrunapp.utils.MFunction;
import com.orm.SugarContext;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ActivityLiveEventList extends AppCompatActivity {
    RecyclerView recyclerView;
    LiveEventListAdapter liveEventListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_event_list);
        SugarContext.init(this);
        setTitle("Live Event");

        recyclerView = findViewById(R.id.rvEventList);


        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);

        List<EventListDB> eventListDBS;
        eventListDBS = EventListDB.getStartedEvents(MFunction.getUnixTimestamp(formattedDate));
        setRvEventList(eventListDBS);




    }

    private void setRvEventList(List<EventListDB> eventListDBS) {
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        liveEventListAdapter = new LiveEventListAdapter(this, eventListDBS, false);
        recyclerView.setAdapter(liveEventListAdapter);
        liveEventListAdapter.notifyDataSetChanged();
    }
}