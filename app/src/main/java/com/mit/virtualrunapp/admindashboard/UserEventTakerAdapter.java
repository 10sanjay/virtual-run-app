package com.mit.virtualrunapp.admindashboard;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventTakerDB;
import com.mit.virtualrunapp.database.UsersDB;
import com.mit.virtualrunapp.helper.UserHelper;

import java.util.List;

public class UserEventTakerAdapter  extends RecyclerView.Adapter<UserEventTakerAdapter.ViewHolder> {
    Activity activity;
    List<EventTakerDB> usersDBS;
    private boolean isFragment = false;


    public UserEventTakerAdapter(Activity activity, List<EventTakerDB> usersDBS, Boolean isFragment) {
        this.activity = activity;
        this.usersDBS = usersDBS;
        this.isFragment = isFragment;
    }

    @Override
    public UserEventTakerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_client_list, viewGroup, false);
        UserEventTakerAdapter.ViewHolder mh = new UserEventTakerAdapter.ViewHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(UserEventTakerAdapter.ViewHolder holder, int position) {
        EventTakerDB details = usersDBS.get(position);
        holder.textViewName.setText(details.getUserName());
        holder.textViewAddress.setVisibility(View.GONE);
        holder.textViewStatus.setVisibility(View.GONE);
        holder.toggleButton.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return usersDBS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName, textViewAddress, textViewStatus;
        ToggleButton toggleButton;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = view.findViewById(R.id.tvName);
            this.textViewAddress = view.findViewById(R.id.tvAddress);
            this.textViewStatus = view.findViewById(R.id.tvStatus);
            this.toggleButton = view.findViewById(R.id.tbStatus);
        }
    }

}
