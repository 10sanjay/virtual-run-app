package com.mit.virtualrunapp.admindashboard;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.JsonElement;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.location.OnCameraTrackingChangedListener;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.utils.MFunction;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


public class PlacePickerActivity extends BaseActivity implements PermissionsListener, OnCameraTrackingChangedListener, OnMapReadyCallback {

    private static final String TAG = "PlacePickerActivity";
    private MapView mapView;

    private MapboxMap mapboxMap;

    private ImageView dropPinView;
    private Context mContext;
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;
    private boolean isInTrackingMode;
    EditText searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        // This contains the MapView in XML and needs to be called after the access token is configured.
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        setContentView(R.layout.activity_placepicker);
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));

        mapView = findViewById(R.id.mapView);

        mapView.onCreate(savedInstanceState);

        searchView = findViewById(R.id.sv_location);

        TextView tvOffline = findViewById(R.id.tv_sugg_text);
        if (!MFunction.isInternetAvailable(mContext)) {
            tvOffline.setVisibility(View.VISIBLE);
        } else {
            tvOffline.setVisibility(View.GONE);
        }

        //latLongs = Client.getAllowedClientsLatLang(MFunction.getMyPrefVal("employee_id", mContext), true);
        double initLat = getIntent().getDoubleExtra("lat", -1);
        double initLong = getIntent().getDoubleExtra("long", -1);

        searchView.setFocusable(false);


        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MFunction.isInternetAvailable(mContext)) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.NAME);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fieldList).build(PlacePickerActivity.this);
                    startActivityForResult(intent, 100);
                } else {
                    Toast.makeText(mContext, "Please check you internet connection ", Toast.LENGTH_LONG).show();
                }
            }
        });



      /*  searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(MFunction.isInternetAvailable(getApplicationContext())){
                    String location= searchView.getQuery().toString();
                    List<Address> addressList = null;
                    if(location != null || !location.equals("")){
                        if(location.equalsIgnoreCase("kathmandu")){
                            location = "katmandu";
                        }
                        Geocoder geocoder = new Geocoder(PlacePickerActivity.this, Locale.ENGLISH);

                        try{

                            addressList = geocoder.getFromLocationName(location,1);
                            if(addressList.size()>0){
                                Address address = addressList.get(0);

                                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                                //   mMap.addMarker(new MarkerOptions().position(latLng).title(location));
                                mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));

                            }
                            else {
                        Toast.makeText(getApplicationContext(),getString(R.string.text_try_another_address),Toast.LENGTH_LONG).show();
                    }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }

                }else {
                    Toast.makeText(getApplicationContext(),"Connect To internet.",Toast.LENGTH_LONG).show();
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS,Place.Field.LAT_LNG,Place.Field.NAME);
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(PlacePickerActivity.this);
                startActivityForResult(intent,100);
                return false;
            }
        });*/

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap tempMapboxMap) {
                mapboxMap = tempMapboxMap;
                mapboxMap.setStyle(Style.OUTDOORS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {

                        createDropPin();
                        createSelectLocationButton();
                        enableLocationComponent(style);


                        OfflineManager offlineManager = OfflineManager.getInstance(mContext);
                        offlineManager.listOfflineRegions(new OfflineManager.ListOfflineRegionsCallback() {
                            @Override
                            public void onList(OfflineRegion[] offlineRegions) {
                                Log.i(TAG, "onList: " + offlineRegions.length);
                                if (offlineRegions.length == 0) return;

                                LatLngBounds bounds = offlineRegions[0].getDefinition().getBounds();
                                double regionZoom = offlineRegions[0].getDefinition().getMinZoom();

                                // Create new camera position
                                LatLng center;
                                if (initLat != -1 && initLong != -1) {
                                    center = new LatLng(initLat, initLong);
                                } else {
                                    center = bounds.getCenter();
                                }
                                CameraPosition cameraPosition = new CameraPosition.Builder()
                                        .target(center)
                                        .zoom(17)
                                        //.zoom(regionZoom)
                                        .build();

                                // Move camera to new position
                                mapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                            }

                            @Override
                            public void onError(String error) {

                            }
                        });
                    }
                });

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


    private void createSelectLocationButton() {
        //noinspection ConstantConditions

        Button selectLocationButton = findViewById(R.id.selectLocationButton);


        selectLocationButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "Location Selected!");
                        if (mapboxMap != null) {
                            LatLng position = mapboxMap.getCameraPosition().target;
                            //LatLng position = getLocationPickerLocation();

                            Log.i(TAG, "onClick: position=" + position.toString());

                            // Convert LatLng coordinates to screen pixel and only query the rendered features.
                            final PointF pixel = mapboxMap.getProjection().toScreenLocation(position);

                            RectF rectF = new RectF(
                                    pixel.x - 30,
                                    pixel.y - 30,
                                    pixel.x + 30,
                                    pixel.y + 30
                            );

                            List<Feature> features = mapboxMap.queryRenderedFeatures(rectF);
                            Bundle bundle = new Bundle();
                            //22.539530, 88.357024
                            //latitude=22.530243335228846, longitude=88.35677826385472


                            if (features.size() > 0) {
                                for (Feature feature : features) {
                                    if (feature.properties() != null) {
                                        JsonElement nameElem = feature.properties().get("name");
                                        if (nameElem != null) {
                                            bundle.putString("name", nameElem.getAsString());
                                            break;
                                        }
                                    }
                                }

                            }


                            bundle.putDouble("latitude", position.getLatitude());
                            bundle.putDouble("longitude", position.getLongitude());
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("bundle", bundle);
                            PlacePickerActivity.this.setResult(RESULT_OK, returnIntent);
                            finish();
                        }
                    }
                }
        );

    }

    private void createDropPin() {

        float density = getResources().getDisplayMetrics().density;

        dropPinView = new ImageView(this);
        dropPinView.setImageResource(R.drawable.ic_droppin);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        params.bottomMargin = (int) (12 * density);
        dropPinView.setLayoutParams(params);

        mapView.addView(dropPinView);
    }

    private void showDropPin() {
        if (dropPinView != null && dropPinView.getVisibility() != View.VISIBLE) {
            dropPinView.setVisibility(View.VISIBLE);
        }
    }

    private void hide(View view) {
        if (view != null) {
            view.setVisibility(View.INVISIBLE);
        }
    }

    private LatLng getLocationPickerLocation() {
        LatLng mapLatLng = mapboxMap.getCameraPosition().target;
        return mapboxMap.getProjection().fromScreenLocation(
                new PointF(dropPinView.getLeft() + (dropPinView.getWidth() / 2), dropPinView.getBottom())
        );
    }


    public void goBack(View view) {
        finish();
    }


    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

// Create and customize the LocationComponent's options
            LocationComponentOptions customLocationComponentOptions = LocationComponentOptions.builder(this)
                    .elevation(5)
                    .accuracyAlpha(.6f)
                    .accuracyColor(Color.RED)
                    .enableStaleState(false)

                    .trackingGesturesManagement(false)
                    .foregroundDrawable(R.drawable.ic_pin)
                    .build();

// Get an instance of the component
            locationComponent = mapboxMap.getLocationComponent();

            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .locationComponentOptions(customLocationComponentOptions)
                            .build();

// Activate with options
            locationComponent.activateLocationComponent(locationComponentActivationOptions);

// Enable to make component visible
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                return;
            }
            locationComponent.setLocationComponentEnabled(true);

// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

// Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);


// Add the camera tracking listener. Fires if the map camera is manually moved.
//            locationComponent.addOnCameraTrackingChangedListener(this);

//            findViewById(R.id.back_to_camera_tracking_mode).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (!isInTrackingMode) {
//                        isInTrackingMode = true;
//                        locationComponent.setCameraMode(CameraMode.TRACKING);
//                        locationComponent.zoomWhileTracking(16f);
////                        Toast.makeText(PlacePickerActivity.this, getString(R.string.tracking_enabled),
////                                Toast.LENGTH_SHORT).show();
//                    } else {
////                      createDropPin();
//                    }
//                }
//            });

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions,int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.text_location_permission_granted, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            Toast.makeText(this, R.string.text_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }

    }

    @Override
    public void onCameraTrackingDismissed() {
        isInTrackingMode = false;

    }

    @Override
    public void onCameraTrackingChanged(int currentMode) {

    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {

    }

    public void moveToCurrentLocation(View view) {

        double lat = mapboxMap.getLocationComponent().getLastKnownLocation().getLatitude();
        double lng = mapboxMap.getLocationComponent().getLastKnownLocation().getLongitude();

        CameraPosition old = mapboxMap.getCameraPosition();
        CameraPosition pos = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                //.zoom(old.zoom)
                .zoom(17)
                .tilt(old.tilt)
                .build();

        //mapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(pos), 1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            Place place = Autocomplete.getPlaceFromIntent(data);

          //  searchView.setText(place.getAddress());
            searchView.setText(place.getAddress());




                Log.d("AjDebug", place.toString());
                LatLng latLng = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                //   mMap.addMarker(new MarkerOptions().position(latLng).title(location));
                mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                LatLng mapLatLng = mapboxMap.getCameraPosition().target;
            Log.d("AjDebug", mapLatLng.toString());

        }

    }
}

