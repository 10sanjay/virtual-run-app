package com.mit.virtualrunapp.admindashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.easywaylocation.EasyWayLocation;
import com.example.easywaylocation.Listener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.activityLogin.LoginActivity;
import com.mit.virtualrunapp.activitymap.MapsActivity;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.databinding.ActivityMainBinding;
import com.mit.virtualrunapp.helper.NewEventHelper;
import com.mit.virtualrunapp.utils.MFunction;
import com.mit.virtualrunapp.utils.UserSessionManager;
import com.orm.SugarContext;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final long LOCATION_REFRESH_TIME = 1000;
    private static final float LOCATION_REFRESH_DISTANCE = 10;
    ActivityMainBinding activityMainBinding;
    private static final int LOCATION_PERMISSIONS_REQUEST_CODE = 34;

    UserSessionManager userSessionManager;


    LocationManager mLocationManager;
    boolean hasLocationPermission = false;
    private Context mContext;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference referenceEventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(activityMainBinding.getRoot());
        setTitle("Virtual Run");
        mContext = this;
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        userSessionManager = new UserSessionManager(this);
        SugarContext.init(mContext);

        fetchCurrentLocation();
        fetchEvents();

        hasLocationPermission = MFunction.hasPermissions(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);
        activityMainBinding.menuMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hasLocationPermission = MFunction.hasPermissions(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);

                if (hasLocationPermission) {
                    if (MFunction.getCurrentLocation(getApplicationContext()) != null) {
                        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                        startActivity(intent);
                    } else {
                        fetchCurrentLocation();
                        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                        startActivity(intent);
                    }

                } else {
                    requestPermissions();
                }
            }
        });

        activityMainBinding.menuEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityEvents.class);
                startActivity(intent);
            }
        });

        activityMainBinding.menuViewUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityUsersList.class);
                startActivity(intent);
            }
        });

        activityMainBinding.menuViewFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ActivityFinalResult.class);
                startActivity(intent);
            }
        });

        activityMainBinding.liveevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date c = Calendar.getInstance().getTime();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                String formattedDate = df.format(c);

                List<EventListDB> eventListDBS;
                eventListDBS = EventListDB.getStartedEvents(MFunction.getUnixTimestamp(formattedDate));

                if(!eventListDBS.isEmpty()){
                    Intent intent = new Intent(getApplicationContext(),ActivityLiveEventList.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(getApplicationContext(),"There are no live event\'s right now.",Toast.LENGTH_LONG).show();
                }

            }
        });


        /*easyWayLocation = new EasyWayLocation(this);
        easyWayLocation.setListener(this);*/
    }

    private void fetchEvents() {
        referenceEventList = FirebaseDatabase.getInstance().getReference("events");

        referenceEventList.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                EventListDB.deleteAll(EventListDB.class);

                for (DataSnapshot postnap : snapshot.getChildren()) {
                    NewEventHelper eventHelper = postnap.getValue(NewEventHelper.class);
                    EventListDB eventListDB = new EventListDB(eventHelper.getEventName(), eventHelper.getStartDate(), eventHelper.getEndDate(), eventHelper.getStartPoint(), eventHelper.getEndPoint(), eventHelper.getStartTime(), eventHelper.getStatus());
                    eventListDB.save();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i("request permission", "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(android.R.id.content),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION ,Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                                    LOCATION_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i("Requesting Permission", "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION ,Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                    LOCATION_PERMISSIONS_REQUEST_CODE);
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            Log.i("checkingLoc","reacher1");

            MFunction.putMyPrefVal("latitude",location.getLatitude()+"",mContext);
            MFunction.putMyPrefVal("longitude",location.getLongitude()+"",mContext);
        }
    };

    private void fetchCurrentLocation() {

        //  LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (mLocationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                return;
            }
            mLocationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.i("checkingLoc","reacher1");
                    if (location != null) {
                        Log.i("checkingLoc","reacher2");

                        MFunction.putMyPrefVal("latitude", location.getLatitude() + "", mContext);
                        MFunction.putMyPrefVal("longitude", location.getLongitude() + "", mContext);
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            }, null);



            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, mLocationListener);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu item) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_setting_admin, item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout:
                userSessionManager.setLogin(false);
                Intent intent1 = new Intent(this, LoginActivity.class);
                startActivity(intent1);
                this.finishAffinity();
                return true;

           /* case R.id.changePassword:
                Intent intent2 = new Intent(this, ChangePassword.class);
                startActivity(intent2);
                return true;
            case R.id.viewHistory:
                Intent i = new Intent(getApplicationContext(), MapHistoryActivity.class);
                startActivity(i);
                return true;
            case R.id.languageChange:
                Intent i1 = new Intent(getApplicationContext(), ChangeLanguage.class);
                startActivity(i1);
                return true;*/
        }
        return true;
    }


}