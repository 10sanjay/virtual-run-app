package com.mit.virtualrunapp.admindashboard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.mit.virtualrunapp.utils.MFunction;


public class BaseActivity extends AppCompatActivity {

    public Context mContext;
    private BroadcastReceiver gpsBroadCastReceiver;
    private boolean prevGpsStatus;
    private AlertDialog gpsAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        createAlertDialog();
        prevGpsStatus = MFunction.isLocationEnabled(mContext);
        manageGpsStatus();

        gpsBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
                    boolean tempStatus = MFunction.isLocationEnabled(mContext);

                    if (prevGpsStatus != tempStatus) {
                        prevGpsStatus = tempStatus;
                        manageGpsStatus();
                    }
                }
            }
        };

        registerReceiver(gpsBroadCastReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
    }

    private void createAlertDialog() {
        gpsAlertDialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Virtual Run App")
                .setMessage("You Should Enable Your Location")
                .setPositiveButton("OK", (dialog, which) -> {
                }).create();

    }

    private void manageGpsStatus() {
        if (prevGpsStatus)
            hideGpsDialog();
        else
            showGpsDialog();
    }

    private void hideGpsDialog() {
        if (gpsAlertDialog.isShowing()) gpsAlertDialog.dismiss();
    }

    private void showGpsDialog() {
        gpsAlertDialog.show();
        gpsAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)));
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(gpsBroadCastReceiver);
        if (gpsAlertDialog.isShowing()) gpsAlertDialog.dismiss();
        super.onDestroy();
    }
}
