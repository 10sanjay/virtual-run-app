package com.mit.virtualrunapp.admindashboard;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.utils.MFunction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;

public class ActivtiyPartiesRoute extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationChangeListener {
    int PLACE_PICKER_REQUEST_START = 100;
    int PLACE_PICKER_REQUEST_END = 101;
    LatLng startLocation, endLocation;
    private GoogleMap mMap, mMapRoute;
    private Context mContext;
    FusedLocationProviderClient fusedLocationProviderClient;
    Location currentlocation;
    EditText searchView;
    SupportMapFragment mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activtiy_parties_route);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        searchView = findViewById(R.id.sv_location);
        currentlocation = MFunction.getCurrentLocation(mContext);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);
        fetchdatalocation();

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.


        mapFragment.getMapAsync(this);

        searchView.setFocusable(false);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.LAT_LNG, Place.Field.NAME);
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fieldList).build(getApplicationContext());
                startActivityForResult(intent, 1010);
            }
        });
    }

    public void fetchdatalocation() {
        @SuppressLint("MissingPermission") Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(location -> {
            if (location != null) {
                currentlocation = location;
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);


            }

        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        //menuInflater.inflate(R.menu.activity_nearby_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }

           /* case R.id.action_sync: {
                fetchData();
                *//*if(MFunction.isInternetAvailable(mContext)){
                    fetchData(true);

                } else {
                    Toast.makeText(mContext, "Internet Not Available.", Toast.LENGTH_SHORT).show();
                }*//*
                return true;
            }*/

            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMapRoute = googleMap;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LatLng latLng = new LatLng(currentlocation.getLatitude(), currentlocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19));
            googleMap.addMarker(markerOptions);
            mMap.setMyLocationEnabled(true);
//            mMapRoute.setMyLocationEnabled(true);

        } else {

            //   MFunction.logToast("no fine location permission");
        }


//        mMap.getUiSettings().setMapToolbarEnabled(true);
//        mMapRoute.getUiSettings().setMapToolbarEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1010 && resultCode == RESULT_OK) {
            Place placeFromIntent = Autocomplete.getPlaceFromIntent(data);

            searchView.setText(placeFromIntent.getAddress());


            LatLng latLng = new LatLng(placeFromIntent.getLatLng().latitude, placeFromIntent.getLatLng().longitude);
            //   mMap.addMarker(new MarkerOptions().position(latLng).title(location));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19));


        }

        if (requestCode == PLACE_PICKER_REQUEST_START) {
            if (resultCode == RESULT_OK) {
                Place place = (Place) PlacePicker.getPlace(data, this);
                startLocation = place.getLatLng();
                Double lati = place.getLatLng().latitude;
                Double longi = place.getLatLng().longitude;
                //   MFunction.logToast(lati + "/" + longi);
                String country = "";
                //String state = "";
                //String city = "";
                String featureName = "";
                String locality = "";
                String subLocality = "";
                String subAdminArea = "";
                String postalCode = "";

                Geocoder geoCoder = new Geocoder(this.getBaseContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geoCoder.getFromLocation(lati, longi, 1);
                    if (addresses.size() > 0) {
                        country = addresses.get(0).getCountryName();//country e.g Nepal
                        locality = addresses.get(0).getLocality();//city e.g Bharaul
                        subAdminArea = addresses.get(0).getSubAdminArea(); // State e.g Koshi
                        postalCode = addresses.get(0).getPostalCode();
                        String choosedAddress = subAdminArea + ", " + locality;
                        //formattedAddress = choosedAddress.replace("null","");
                    }
                } catch (IOException ex) {
                    //ex.printStackTrace();
                }
            }
        }

        if (requestCode == PLACE_PICKER_REQUEST_END) {
            if (resultCode == RESULT_OK) {
                Place place = (Place) PlacePicker.getPlace(data, this);
                Double lati = place.getLatLng().latitude;
                Double longi = place.getLatLng().longitude;
                endLocation = place.getLatLng();
                //     MFunction.logToast(lati + "/" + longi);
                String country = "";
                //String state = "";
                //String city = "";
                String featureName = "";
                String locality = "";
                String subLocality = "";
                String subAdminArea = "";
                String postalCode = "";

                Geocoder geoCoder = new Geocoder(this.getBaseContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geoCoder.getFromLocation(lati, longi, 1);
                    if (addresses.size() > 0) {
                        country = addresses.get(0).getCountryName();//country e.g Nepal
                        locality = addresses.get(0).getLocality();//city e.g Bharaul
                        subAdminArea = addresses.get(0).getSubAdminArea(); // State e.g Koshi
                        postalCode = addresses.get(0).getPostalCode();
                        String choosedAddress = subAdminArea + ", " + locality;
                    }
                } catch (IOException ex) {
                    //ex.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onMyLocationChange(Location location) {
        // MFunction.logToast("Location Changed");
    }


    public void showProgressBar() {
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        findViewById(R.id.progressBar).setVisibility(View.GONE);

    }

}
