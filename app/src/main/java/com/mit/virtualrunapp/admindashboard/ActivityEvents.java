package com.mit.virtualrunapp.admindashboard;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;


import com.google.android.libraries.places.api.Places;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.databinding.ActivityEventsBinding;
import com.mit.virtualrunapp.helper.NewEventHelper;
import com.mit.virtualrunapp.utils.MFunction;
import com.orm.SugarContext;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ActivityEvents extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private AppBarConfiguration appBarConfiguration;
    private ActivityEventsBinding binding;
    Dialog newEventDialog;
    ProgressDialog progressDialog;

    String TAG = "ActivityEvents";

    TextView btnApply, btnClose;
    TextView tvStartDate, tvStartTime, tvEndDate, tvStartPoint, tvFinalPoint;
    EditText editTextEventName;

    int PLACE_PICKER_REQUEST = 1;

    String pointSelected = null;
    Boolean isStartDate = true;

    String holderStartpoint = "";
    String holderEndPoint = "";

    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;

    RecyclerView recyclerView;
    EventListAdapter eventListAdapter;

    String placeId = "INSERT_PLACE_ID_HERE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEventsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setTitle("Events");
        setSupportActionBar(binding.toolbar);
        SugarContext.init(this);
        Places.initialize(getApplicationContext(),getString(R.string.google_maps_key));

        recyclerView = findViewById(R.id.rvEventList);

        progressDialog = new ProgressDialog(this);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showNewEventDialog();
            }
        });

        reference = FirebaseDatabase.getInstance().getReference("events");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                EventListDB.deleteAll(EventListDB.class);

                for (DataSnapshot postnap : snapshot.getChildren()) {
                    NewEventHelper eventHelper = postnap.getValue(NewEventHelper.class);
                    EventListDB eventListDB = new EventListDB(eventHelper.getEventName(), eventHelper.getStartDate(), eventHelper.getEndDate(), eventHelper.getStartPoint(), eventHelper.getEndPoint(), eventHelper.getStartTime(), eventHelper.getStatus());
                    eventListDB.save();
                }

                List<EventListDB> eventListDBS;
                eventListDBS = EventListDB.getAllEventList();
                setRvEventList(eventListDBS);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void setRvEventList(List<EventListDB> eventListDBS) {
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        eventListAdapter = new EventListAdapter(this, eventListDBS, false);
        recyclerView.setAdapter(eventListAdapter);
        eventListAdapter.notifyDataSetChanged();
    }

    private void showNewEventDialog() {


        View view = getLayoutInflater().inflate(R.layout.layout_dialog_create_events, null);
        newEventDialog = new Dialog(this, R.style.Theme_AppCompat_Light_Dialog_Alert);
        newEventDialog.setCancelable(false);
        newEventDialog.setContentView(view);

        btnApply = view.findViewById(R.id.btn_apply);
        btnClose = view.findViewById(R.id.btn_close);
        tvStartDate = view.findViewById(R.id.tvDate);
        tvStartTime = view.findViewById(R.id.tvStartTime);
        tvEndDate = view.findViewById(R.id.tvEndDate);
        tvStartPoint = view.findViewById(R.id.tvStartPoint);
        tvFinalPoint = view.findViewById(R.id.tvEndPoint);
        editTextEventName = view.findViewById(R.id.etEventName);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newEventDialog.dismiss();
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MFunction.isInternetAvailable(getApplicationContext())) {
                    if (validate()) {

                        String eventName, startPoint, endPoint, startTime, startDate, endDate;
                        eventName = editTextEventName.getText().toString();
                        startPoint = holderStartpoint;
                        endPoint = holderEndPoint;
                        startTime = tvStartTime.getText().toString();
                        startDate = tvStartDate.getText().toString();
                        endDate = tvEndDate.getText().toString();
                        progressDialog.setMessage("Please wait... Creating Event...");

                        showDialog();

                        firebaseDatabase = FirebaseDatabase.getInstance();
                        reference = firebaseDatabase.getReference("events");

                        NewEventHelper newEventHelper = new NewEventHelper(eventName, startPoint, endPoint, startTime, startDate, endDate, "Active");
                        reference.child(eventName).setValue(newEventHelper, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                                hideDialog();
                                Toast.makeText(getApplicationContext(), "Event Successfully Created.", Toast.LENGTH_LONG).show();
                                newEventDialog.dismiss();
                            }
                        });


                    } else {
                        Toast.makeText(getApplicationContext(), "Please fill all data", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection.", Toast.LENGTH_LONG).show();
                }

            }
        });

        tvStartPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pointSelected = "start";
                Intent intent=new Intent(ActivityEvents.this,PlacePickerActivity.class);
                startActivityForResult(intent,PLACE_PICKER_REQUEST);
            }
        });

        tvFinalPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holderStartpoint == null || holderStartpoint.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please select start point first.",Toast.LENGTH_LONG).show();
                } else {
                    pointSelected = "final";
                    Intent intent=new Intent(ActivityEvents.this,PlacePickerActivity.class);
                    startActivityForResult(intent,PLACE_PICKER_REQUEST);
                }

            }
        });

        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                TimePickerDialog dialog = new TimePickerDialog(ActivityEvents.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override


                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        String time = i + ":" + i1 /*+ ":" + "00"*/;

                        tvStartTime.setText(time);


                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                dialog.show();
            }
        });

        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                isStartDate = true;

                Calendar now = Calendar.getInstance();
                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH);
                int day = now.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ActivityEvents.this,
                        year,
                        month,
                        day
                );
                dpd.setMinDate(now);
                dpd.setLocale(Locale.US);
                dpd.show(getFragmentManager(), "DatepickerdialogStart");

            }
        });

        tvEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvStartDate.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please select start date first.", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        isStartDate = false;

                        Calendar now = Calendar.getInstance();
                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = formatter.parse(tvStartDate.getText().toString());
                        int year = now.get(Calendar.YEAR);
                        int month = now.get(Calendar.MONTH);
                        int day = now.get(Calendar.DAY_OF_MONTH);


                        DatePickerDialog dpd = DatePickerDialog.newInstance(
                                ActivityEvents.this,
                                year,
                                month,
                                day
                        );
                        dpd.setMinDate(MFunction.toCalendar(date));
                        dpd.setLocale(Locale.US);
                        dpd.show(getFragmentManager(), "DatepickerdialogStart");
                    } catch (Exception e) {

                    }

                }
            }
        });


        newEventDialog.show();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String date = MFunction.getDate(calendar.getTimeInMillis());

        if (isStartDate) {
            tvStartDate.setText(date);
        } else {
            tvEndDate.setText(date);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {

                Bundle bundle = data.getExtras().getBundle("bundle");
                double latituded = bundle.getDouble("latitude");
                double longituded = bundle.getDouble("longitude");

                Log.i("checkLATS",latituded+"");
                Log.i("checkLATS",longituded+"");





               /* StringBuilder stringBuilder = new StringBuilder();*/
                String latitude = latituded + "";
                String longitude = longituded + "";
               /* stringBuilder.append("LATITUDE :");
                stringBuilder.append(latitude);
                stringBuilder.append("\n");
                stringBuilder.append("LONGITUDE :");
                stringBuilder.append(longitude);*/

                if(!holderStartpoint.isEmpty() && !pointSelected.equals("start")){
                    String[] intPoint = holderStartpoint.split(",");
                    if((MFunction.calculateDistance(Double.valueOf(intPoint[0]),Double.valueOf(intPoint[1]),latituded,longituded)>(1000*1000)) || (MFunction.calculateDistance(Double.valueOf(intPoint[0]),Double.valueOf(intPoint[1]),latituded,longituded)<(1000))){
                        Toast.makeText(getApplicationContext(), "Distance between points start and finish line must be within 1KM to 1000KM.",Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(ActivityEvents.this,PlacePickerActivity.class);


                        startActivityForResult(intent,PLACE_PICKER_REQUEST);

                    }
                }
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latituded, longituded, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    if (pointSelected.equals("start")) {
                        tvStartPoint.setText(address);
                        holderStartpoint = latituded + ","+longituded;

                    } else {
                        tvFinalPoint.setText(address);
                        holderEndPoint =latituded+ ","+ longituded ;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    private Boolean validate() {
        boolean valid = true;

        if (editTextEventName.getText().toString().trim().isEmpty()) {
            valid = false;
        }

        if (tvEndDate.getText().equals(R.string.end_date)) {
            valid = false;
        }

        if (tvStartDate.getText().equals(R.string.start_date)) {
            valid = false;
        }

        if (tvStartTime.getText().equals(R.string.start_time)) {
            valid = false;
        }

        if (tvStartPoint.getText().equals(R.string.start_point)) {
            valid = false;
        }

        if (tvFinalPoint.getText().equals(R.string.end_point)) {
            valid = false;
        }

        return valid;

    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}