package com.mit.virtualrunapp.admindashboard;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.database.EventTakerDB;
import com.mit.virtualrunapp.database.UserEventRunTracker;
import com.mit.virtualrunapp.databinding.ActivityMapsLiveEventBinding;
import com.mit.virtualrunapp.helper.EventTakerHelper;
import com.mit.virtualrunapp.helper.UserRunTracker;
import com.mit.virtualrunapp.utils.UserSessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MapsActivityLiveEvent extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsLiveEventBinding binding;

    DatabaseReference referenceLiveTracker, referenceUsersTakenPart;
    List<UserEventRunTracker> userEventRunTrackerList;
    List<EventTakerDB> eventTakerDBList;

    private MarkerOptions options = new MarkerOptions();
    List<LatLng> latLngList = new ArrayList<>();
    List<String> titles = new ArrayList<>();

    String eventName;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsLiveEventBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        showDialog();

        eventName = getIntent().getStringExtra("eventname");
        fetchEventTaker();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    private void fetchEventTaker() {
        referenceUsersTakenPart = FirebaseDatabase.getInstance().getReference("eventtaker");

        referenceUsersTakenPart.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                EventTakerDB.deleteAll(EventTakerDB.class);
                UserEventRunTracker.deleteAll(UserEventRunTracker.class);

                for (DataSnapshot postnap : snapshot.getChildren()) {
                    EventTakerHelper eventTakerHelper = postnap.getValue(EventTakerHelper.class);
                    EventTakerDB eventListDB = new EventTakerDB(eventTakerHelper.getUsername(), eventTakerHelper.getEventname(), eventTakerHelper.getStatus());
                    eventListDB.save();
                }

                eventTakerDBList = EventTakerDB.getEventTakerByEventName(eventName);

                if (!eventTakerDBList.isEmpty()) {

                    for (int i = 0; i < eventTakerDBList.size(); i++) {
                        fetchLiveTracker(eventTakerDBList.get(i).getEventName() + eventTakerDBList.get(i).getUserName());
                    }

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 3s = 3000ms


                            for (int q = 0; q < eventTakerDBList.size(); q++) {
                                if (!UserEventRunTracker.getLatitudeByUserName(eventTakerDBList.get(q).getUserName()).isEmpty()) {
                                    LatLng latLngs = new LatLng(Double.parseDouble(UserEventRunTracker.getLatitudeByUserName(eventTakerDBList.get(q).getUserName())), Double.parseDouble(UserEventRunTracker.getLongitudeByUserName(eventTakerDBList.get(q).getUserName())));
                                    latLngList.add(latLngs);
                                    titles.add(eventTakerDBList.get(q).getUserName());
                                }
                            }

                            for (int p = 0; p < latLngList.size(); p++) {
                                options.position(latLngList.get(p));
                                options.title(titles.get(p));
                                // options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_green));
                                mMap.addMarker(options);

                            }
                            hideDialog();
                            mMap.setOnMarkerClickListener(marker -> {

                                fetchLiveTracker(eventName + marker.getTitle());

                                userEventRunTrackerList = UserEventRunTracker.getrunListByusernameeventname(marker.getTitle(), eventName);

                                double distance = 0;
                                if (userEventRunTrackerList != null) {
                                    for (int i = 0; i < userEventRunTrackerList.size(); i++) {
                                        distance += Double.parseDouble(userEventRunTrackerList.get(i).getDistance());
                                    }
                                    if (distance != 0) {
                                        distance = distance / 1000;
                                    }
                                }

                                if (marker.getTitle().equals("Start Line") || marker.getTitle().equals("Finish Line")) {

                                } else {
                                    Toast.makeText(getApplicationContext(), marker.getTitle() + " has covered " + distance + "Km", Toast.LENGTH_LONG).show();
                                }
                                return false;
                            });

                        }
                    }, 3000);


                } else {
                    hideDialog();
                    Toast.makeText(getApplicationContext(), "No one has taken part in this event.", Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void fetchLiveTracker(String eventUser) {
        referenceLiveTracker = FirebaseDatabase.getInstance().getReference("eventruntracker");


        referenceLiveTracker.child(eventUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                UserEventRunTracker.deleteAll(UserEventRunTracker.class);

                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    UserRunTracker userRunTracker = dataSnapshot.getValue(UserRunTracker.class);
                    UserEventRunTracker userEventRunTracker = new UserEventRunTracker(userRunTracker.getLatitude(), userRunTracker.getLongitude(), userRunTracker.getDistance(), userRunTracker.getSpeed(), userRunTracker.getEventname(), userRunTracker.getUsername());
                    userEventRunTracker.save();

                }

            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        String[] intPoint = EventListDB.getStartPoint(eventName).split(",");
        String[] finalPoint = EventListDB.getEndPoint(eventName).split(",");


        LatLng startPoint = new LatLng(Double.parseDouble(intPoint[0]), Double.parseDouble(intPoint[1]));
        LatLng endPoint = new LatLng(Double.parseDouble(finalPoint[0]), Double.parseDouble(finalPoint[1]));
        mMap.addMarker(new MarkerOptions().position(startPoint).title("Start Line").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_blue)));
        mMap.addMarker(new MarkerOptions().position(endPoint).title("Finish Line").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_green)));

        LatLngBounds.builder().include(startPoint).include(endPoint).build().getCenter();





        /* // Add a marker in Sydney and move the camera
         *//* LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));*//*
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}