package com.mit.virtualrunapp.admindashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.UsersDB;
import com.mit.virtualrunapp.helper.UserHelper;
import com.orm.SugarContext;
import com.orm.SugarRecord;

import java.util.List;

public class ActivityUsersList extends AppCompatActivity {

    RecyclerView recyclerView;
    DatabaseReference reference;

    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);
        setTitle("User Lists");

        SugarContext.init(this);
        recyclerView = findViewById(R.id.rvClientList);

        reference = FirebaseDatabase.getInstance().getReference("users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                UsersDB.deleteAll(UsersDB.class);

                for (DataSnapshot postNap : snapshot.getChildren()) {
                    UserHelper userHelper = postNap.getValue(UserHelper.class);
                    if (userHelper.getMode().equals("normal")) {

                        UsersDB usersDB = new UsersDB(userHelper.getEmail(), userHelper.getMode(), userHelper.getPhone(), userHelper.getUsername(), userHelper.getStatus(), userHelper.getAddress(), userHelper.getPassword());
                        usersDB.save();
                    }
                }
                Log.i("checking","Reached List");

                List<UsersDB> usersDBS;
                usersDBS = UsersDB.getAllUsers();

                Log.i("checking",usersDBS.size()+"");

                setRvUserList(usersDBS);

            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });





    }

    private void setRvUserList(List<UsersDB> usersDBS) {
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        userListAdapter = new UserListAdapter(this, usersDBS, false);
        recyclerView.setAdapter(userListAdapter);
        userListAdapter.notifyDataSetChanged();
    }
}