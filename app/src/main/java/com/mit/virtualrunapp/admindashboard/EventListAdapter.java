package com.mit.virtualrunapp.admindashboard;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.database.UsersDB;
import com.mit.virtualrunapp.helper.UserHelper;

import java.util.List;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.ViewHolder> {

    Activity activity;
    List<EventListDB> eventListDBS;
    private boolean isFragment = false;

    public EventListAdapter(Activity activity, List<EventListDB> eventListDBS, Boolean isFragment) {
        this.activity = activity;
        this.eventListDBS = eventListDBS;
        this.isFragment = isFragment;
    }

    @Override
    public EventListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_event_list, viewGroup, false);
        EventListAdapter.ViewHolder mh = new EventListAdapter.ViewHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(EventListAdapter.ViewHolder holder, int position) {
        EventListDB details = eventListDBS.get(position);
        holder.textViewName.setText(details.getName());
        holder.textViewStartDate.setText(details.getStartDate());
        holder.textViewEndDate.setText(details.getEndDate());

        holder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity.getApplicationContext(),ActivityEventDescription.class);
                intent.putExtra("eventName",details.getName());
                intent.putExtra("startDate",details.getStartDate());
                intent.putExtra("endDate",details.getEndDate());
                intent.putExtra("startTime",details.getStartTime());
                intent.putExtra("startPoint",details.getStartPoint());
                intent.putExtra("endPoint",details.getEndPoint());
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return eventListDBS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName, textViewStartDate, textViewEndDate;
        LinearLayout layoutMain;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = view.findViewById(R.id.tvName);
            this.textViewStartDate = view.findViewById(R.id.tvStartDate);
            this.textViewEndDate = view.findViewById(R.id.tvEndDate);
            this.layoutMain = view.findViewById(R.id.holderMain);
        }
    }

}
