package com.mit.virtualrunapp.admindashboard;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.database.EventTakerDB;
import com.mit.virtualrunapp.database.FinalScoreDB;
import com.mit.virtualrunapp.database.UserEventRunTracker;
import com.mit.virtualrunapp.services.LocationUpdatesService;

import java.util.List;

public class FinalScoreAdapter extends RecyclerView.Adapter<FinalScoreAdapter.ViewHolder> {

    Activity activity;
    List<FinalScoreDB> eventTakerDBS;
    private boolean isFragment = false;
    private LocationUpdatesService mService = null;
    private static final int LOCATION_PERMISSIONS_REQUEST_CODE = 34;

    List<UserEventRunTracker> userEventRunTrackers;

    boolean hasLocationPermission = false;

    public FinalScoreAdapter(Activity activity, List<FinalScoreDB> eventTakerDBS, Boolean isFragment) {
        this.activity = activity;
        this.eventTakerDBS = eventTakerDBS;
        this.isFragment = isFragment;
    }

    @Override
    public FinalScoreAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_final_score, viewGroup, false);
        FinalScoreAdapter.ViewHolder mh = new FinalScoreAdapter.ViewHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(FinalScoreAdapter.ViewHolder holder, int position) {
        FinalScoreDB details = eventTakerDBS.get(position);

        userEventRunTrackers = UserEventRunTracker.getrunListByusernameeventname(details.getUserName(),details.getEventName());

        holder.textViewName.setText(eventTakerDBS.get(position).getUserName());


        if(userEventRunTrackers != null){
            float distance = 0;
            float speed = 0;
            for (int i = 0; i < userEventRunTrackers.size(); i++){
                distance += Float.parseFloat(userEventRunTrackers.get(i).getDistance());
                if(userEventRunTrackers.get(i).getSpeed().equals("NaN") || userEventRunTrackers.get(i).getSpeed().equals("Infinity")){

                } else {
                    speed += Float.parseFloat(userEventRunTrackers.get(i).getSpeed());
                }
            }
            speed = speed/userEventRunTrackers.size();

            if(distance != 0){
                distance = distance / 1000;
            }

            holder.textViewAverageSpeed.setText("Average Speed :- "+speed+"m/s");
            holder.textViewDistance.setText("Distance :- "+distance+"Km");


        } else {
            holder.textViewDistance.setText("Distance :- "+"N/A");
            holder.textViewAverageSpeed.setText("Average Speed :- "+"N/A");
        }





    }

    @Override
    public int getItemCount() {
        return eventTakerDBS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName, textViewStartDate, textViewEndDate, textViewAverageSpeed, textViewDistance;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = view.findViewById(R.id.tvName);
            this.textViewStartDate = view.findViewById(R.id.tvStartDate);
            this.textViewEndDate = view.findViewById(R.id.tvEndDate);
            this.textViewAverageSpeed= view.findViewById(R.id.tvspeed);
            this.textViewDistance = view.findViewById(R.id.tvdistance);
        }
    }






    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i("request permission", "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    activity.findViewById(android.R.id.content),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION ,Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                                    LOCATION_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i("Requesting Permission", "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION ,Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                    LOCATION_PERMISSIONS_REQUEST_CODE);
        }
    }

}
