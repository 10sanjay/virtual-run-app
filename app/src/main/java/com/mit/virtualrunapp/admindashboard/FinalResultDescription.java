package com.mit.virtualrunapp.admindashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventTakerDB;
import com.mit.virtualrunapp.database.FinalScoreDB;
import com.mit.virtualrunapp.database.UserEventRunTracker;
import com.mit.virtualrunapp.helper.EventTakerHelper;
import com.mit.virtualrunapp.helper.UserRunTracker;
import com.orm.SugarContext;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FinalResultDescription extends AppCompatActivity {
    ProgressDialog progressDialog;

    DatabaseReference referenceLiveTracker, referenceUsersTakenPart;
    List<EventTakerDB> eventTakerDBList;
    List<FinalScoreDB> finalScoreDBS;

    FinalScoreAdapter finalScoreAdapter;
    RecyclerView recyclerView;

    String event_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_result_description);
        setTitle("Final Score");
        SugarContext.init(this);
        progressDialog = new ProgressDialog(this);

        event_name = getIntent().getStringExtra("eventname");


        recyclerView = findViewById(R.id.rvEventList);
        fetchEventTaker();

    }

    private void setRvEventList(List<FinalScoreDB> eventTakerDBS) {
        hideDialog();
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        finalScoreAdapter = new FinalScoreAdapter(this, finalScoreDBS, false);
        recyclerView.setAdapter(finalScoreAdapter);
        finalScoreAdapter.notifyDataSetChanged();
    }

    private void fetchEventTaker() {
        progressDialog.setMessage("Please wait");
        showDialog();
        referenceUsersTakenPart = FirebaseDatabase.getInstance().getReference("eventtaker");

        referenceUsersTakenPart.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                EventTakerDB.deleteAll(EventTakerDB.class);
                UserEventRunTracker.deleteAll(UserEventRunTracker.class);

                for (DataSnapshot postnap : snapshot.getChildren()) {
                    EventTakerHelper eventTakerHelper = postnap.getValue(EventTakerHelper.class);
                    EventTakerDB eventListDB = new EventTakerDB(eventTakerHelper.getUsername(), eventTakerHelper.getEventname(), eventTakerHelper.getStatus());
                    eventListDB.save();
                }

                eventTakerDBList = EventTakerDB.getEventTakerByEventName(event_name);

                if(!eventTakerDBList.isEmpty()){

                    for (int i = 0; i< eventTakerDBList.size(); i++){
                        fetchLiveTracker(eventTakerDBList.get(i).getEventName()+eventTakerDBList.get(i).getUserName());
                    }

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            List<UserEventRunTracker> userEventRunTrackers;
                            FinalScoreDB.deleteAll(FinalScoreDB.class);
                            for (int g=0;g<eventTakerDBList.size();g++){
                                userEventRunTrackers = UserEventRunTracker.getrunListByusernameeventname(eventTakerDBList.get(g).getUserName(),eventTakerDBList.get(g).getEventName());
                                float distance = 0;
                                float speed = 0;
                                if( userEventRunTrackers != null ){
                                    for (int i = 0; i < userEventRunTrackers.size(); i++){
                                        distance += Float.parseFloat(userEventRunTrackers.get(i).getDistance());
                                        speed += Float.parseFloat(userEventRunTrackers.get(i).getSpeed());
                                    }
                                    speed = speed/userEventRunTrackers.size();
                                }
                                new FinalScoreDB(eventTakerDBList.get(g).getUserName(),event_name,distance,speed);
                            }
                            finalScoreDBS = FinalScoreDB.getFinalScoreBySpeed(event_name);
                            setRvEventList(finalScoreDBS);

                        }
                    }, 3000);
                } else {
                    hideDialog();
                    Toast.makeText(getApplicationContext(),"No one has taken part in this event.",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void fetchLiveTracker(String eventUser) {
        referenceLiveTracker = FirebaseDatabase.getInstance().getReference("eventruntracker");
        referenceLiveTracker.child(eventUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {

                for (DataSnapshot dataSnapshot :  snapshot.getChildren()){

                    UserRunTracker userRunTracker = dataSnapshot.getValue(UserRunTracker.class);
                    UserEventRunTracker userEventRunTracker = new UserEventRunTracker(userRunTracker.getLatitude(),userRunTracker.getLongitude(),userRunTracker.getDistance(),userRunTracker.getSpeed(),userRunTracker.getEventname(),userRunTracker.getUsername());
                    userEventRunTracker.save();

                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
    }
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }


}