package com.mit.virtualrunapp.admindashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.EventListDB;
import com.mit.virtualrunapp.database.EventTakerDB;
import com.mit.virtualrunapp.helper.EventTakerHelper;
import com.mit.virtualrunapp.helper.NewEventHelper;
import com.mit.virtualrunapp.utils.MFunction;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ActivityEventDescription extends AppCompatActivity implements OnMapReadyCallback {

    String eventName, startDate, endDate, startTime, startPoint, endPoint;

    TextView textViewEName, textViewSDate, textViewEDate, textViewSTime, textViewSPoint, textViewEPoint;

    RecyclerView recyclerView;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;
    UserEventTakerAdapter userEventTakerAdapter;
    SupportMapFragment mapView;

    Double sLat, sLong, eLat, eLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_description);

        eventName = getIntent().getStringExtra("eventName");
        startDate = getIntent().getStringExtra("startDate");
        endDate = getIntent().getStringExtra("endDate");
        startTime = getIntent().getStringExtra("startTime");
        startPoint = getIntent().getStringExtra("startPoint");
        endPoint = getIntent().getStringExtra("endPoint");

        textViewEName = findViewById(R.id.tvEventName);
        textViewSDate = findViewById(R.id.tvStartDate);
        textViewEDate = findViewById(R.id.tvEndDate);
        textViewSTime = findViewById(R.id.tvStartTime);
        textViewSPoint = findViewById(R.id.tvStartPoint);
        textViewEPoint = findViewById(R.id.tvEndPoint);

        mapView = (SupportMapFragment) getSupportFragmentManager() .findFragmentById(R.id.mapView);

        String[] intP = startPoint.split(",");
        String[] finalP = endPoint.split(",");

        sLat= Double.valueOf(intP[0]);
        sLong = Double.valueOf(intP[1]);

        eLat = Double.valueOf(finalP[0]);
        eLong= Double.valueOf(finalP[1]);

        recyclerView = findViewById(R.id.rvUserTakePart);



        textViewEName.setText(eventName);
        textViewSDate.setText(startDate);
        textViewEDate.setText(endDate);
        textViewSTime.setText(startTime);
        textViewSPoint.setText(getPlaceName(intP[0],intP[1],this));
        textViewEPoint.setText(getPlaceName(finalP[0],finalP[1],this));

        reference = FirebaseDatabase.getInstance().getReference("eventtaker");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                EventTakerDB.deleteAll(EventTakerDB.class);

                for (DataSnapshot postnap : snapshot.getChildren()) {
                    EventTakerHelper eventTakerHelper = postnap.getValue(EventTakerHelper.class);
                    EventTakerDB eventListDB = new EventTakerDB(eventTakerHelper.getUsername(), eventTakerHelper.getEventname(), eventTakerHelper.getStatus());
                    eventListDB.save();
                }

                List<EventTakerDB> eventListDBS;
                eventListDBS = EventTakerDB.getEventTakerByEventName(eventName);
                setRvEventList(eventListDBS);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mapView.getMapAsync(this);

    }
    private void setRvEventList(List<EventTakerDB> eventListDBS) {
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        userEventTakerAdapter = new UserEventTakerAdapter(this, eventListDBS, false);
        recyclerView.setAdapter(userEventTakerAdapter);
        userEventTakerAdapter.notifyDataSetChanged();
    }



    public static String getPlaceName(String latitude, String longitude, Context context){
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        String address = "";
        try {

            addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();


        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        // LatLng sydney = new LatLng(currentlocation.getLatitude(), currentlocation.getLongitude());
        LatLng sydney = new LatLng(sLat, sLong);
        LatLng sydney2 = new LatLng(eLat, eLong);
        googleMap.addMarker(new MarkerOptions().position(sydney).title(getPlaceName(sLat+"",sLong+"",this)));
        googleMap.addMarker(new MarkerOptions().position(sydney2).title(getPlaceName(eLat+"",eLong+"",this)));
        // mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(20).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


    }
}