package com.mit.virtualrunapp.admindashboard;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mit.virtualrunapp.R;
import com.mit.virtualrunapp.database.UsersDB;
import com.mit.virtualrunapp.helper.UserHelper;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    Activity activity;
    List<UsersDB> usersDBS;
    private boolean isFragment = false;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;

    public UserListAdapter(Activity activity, List<UsersDB> usersDBS, Boolean isFragment) {
        this.activity = activity;
        this.usersDBS = usersDBS;
        this.isFragment = isFragment;
    }

    @Override
    public UserListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_client_list, viewGroup, false);
        ViewHolder mh = new ViewHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(UserListAdapter.ViewHolder holder, int position) {
        UsersDB details = usersDBS.get(position);
        holder.textViewName.setText(details.getUserName());
        holder.textViewAddress.setText(details.getUserAddress());
        holder.textViewStatus.setText(details.getUserStatus());

        if(details.getUserStatus().equals("Active")){
            holder.toggleButton.setChecked(true);
        } else {
            holder.toggleButton.setChecked(false);
        }

        holder.toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firebaseDatabase = FirebaseDatabase.getInstance();
                reference = firebaseDatabase.getReference("users");

                UserHelper userHelper = new UserHelper(details.getUserName(),details.getEmail(),details.getUserPassword(),details.getPhone(),details.getMode(),holder.toggleButton.getText().toString(),details.getUserAddress());
                reference.child(details.getUserName()).setValue(userHelper);

            }
        });

    }

    @Override
    public int getItemCount() {
        return usersDBS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName, textViewAddress, textViewStatus;
        ToggleButton toggleButton;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = view.findViewById(R.id.tvName);
            this.textViewAddress = view.findViewById(R.id.tvAddress);
            this.textViewStatus = view.findViewById(R.id.tvStatus);
            this.toggleButton = view.findViewById(R.id.tbStatus);
        }
    }

}
