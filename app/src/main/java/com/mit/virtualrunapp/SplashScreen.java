package com.mit.virtualrunapp;

import android.annotation.SuppressLint;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.mit.virtualrunapp.activityLogin.LoginActivity;
import com.mit.virtualrunapp.admindashboard.MainActivity;
import com.mit.virtualrunapp.databinding.ActivitySplashScreenBinding;
import com.mit.virtualrunapp.userdashboard.IndexActivity;
import com.mit.virtualrunapp.utils.UserSessionManager;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreen extends AppCompatActivity {


    private View mContentView;

    private static int SPLASH_TIME_OUT = 3000;
    Intent intent;
    UserSessionManager userSessionManager;



    private boolean mVisible;

    private ActivitySplashScreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivitySplashScreenBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        userSessionManager = new UserSessionManager(this);

        mVisible = true;
        mContentView = binding.fullscreenContent;

        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                goNext();
            }
        }, SPLASH_TIME_OUT);



    }

    private void goNext() {
        if(userSessionManager.isLoggedIn()){
            String user_mode = userSessionManager.getUserMode();

            if(user_mode.equals("admin")){
                intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                intent = new Intent(getApplicationContext(), IndexActivity.class);
                startActivity(intent);
                finish();
            }


        } else {
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }



}