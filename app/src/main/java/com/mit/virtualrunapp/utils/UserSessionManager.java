package com.mit.virtualrunapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSessionManager {

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;


    // Shared preferences file name
    private static final String PREF_NAME = "anushree_traders";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String USER_MODE = "user_mode";
    private static final String USER_NAME = "user_name";
    private static final String USER_PHONE = "user_phone";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_ADDRESS = "user_address";
    private static final String USER_RUNNING = "user_running";
    private static final String USER_RUNNING_EVENT = "user_running_event";

    private static final String USER_PREVIOUSE_LAT = "userlat";
    private static final String USER_PREVIOUSE_LONG = "userlong";

    private static final String USER_TIME_LAST_LOCATION_TRACK = "usertimetrack";

    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setUserRunning(boolean isRunning){
        editor.putBoolean(USER_RUNNING,isRunning);
        editor.commit();
    }

    public boolean isUserRunning(){return pref.getBoolean(USER_RUNNING,false);}


    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.commit();
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void setUserMode(String userMode){
        editor.putString(USER_MODE, userMode);
        editor.commit();
    }

    public String getUserMode(){
        return pref.getString(USER_MODE,null);
    }


    public void setUserName(String userName){
        editor.putString(USER_NAME, userName);
        editor.commit();
    }

    public String getUserName(){
        return pref.getString(USER_NAME,null);
    }

    public void setUserPhone(String userId){
        editor.putString(USER_PHONE, userId);
        editor.commit();
    }

    public String getUserPhone(){
        return pref.getString(USER_PHONE,null);
    }

    public String getUserAddress(){
        return pref.getString(USER_ADDRESS,null);
    }

    public void setUserAddress(String userAddress){
        editor.putString(USER_ADDRESS,userAddress);
        editor.commit();
    }

    public String getUserRunningEvent(){
        return pref.getString(USER_RUNNING_EVENT,null);
    }

    public void setUserRunningEvent(String userRunningEvent){
        editor.putString(USER_RUNNING_EVENT,userRunningEvent);
        editor.commit();
    }

    public String getPreviousLat(){
        return pref.getString(USER_PREVIOUSE_LAT, null);
    }

    public void setUserPreviouseLat(String userPreviouseLat){
        editor.putString(USER_PREVIOUSE_LAT,userPreviouseLat);
        editor.commit();
    }

    public String getPreviousLong(){
        return pref.getString(USER_PREVIOUSE_LONG, null);
    }

    public void setUserPreviouseLong(String  userPreviouseLong){
        editor.putString(USER_PREVIOUSE_LONG,userPreviouseLong);
        editor.commit();
    }


    public void setUserEmail(String userEmail){
        editor.putString(USER_EMAIL,userEmail);
        editor.commit();
    }

    public String getUserTimeLastLocationTrack(){
        return pref.getString(USER_TIME_LAST_LOCATION_TRACK,null);
    }

    public void setUserTimeLastLocationTrack(String userTimeLastLocationTrack){
        editor.putString(USER_TIME_LAST_LOCATION_TRACK,userTimeLastLocationTrack);
        editor.commit();
    }

    public String getUserEmail(){return pref.getString(USER_EMAIL,null);}



}
