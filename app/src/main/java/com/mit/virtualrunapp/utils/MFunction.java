package com.mit.virtualrunapp.utils;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.mit.virtualrunapp.R;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MFunction {

    private static final String TAG = MFunction.class.getSimpleName();

    public static boolean isInternetAvailable(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null) {
            Log.d(TAG, "No Network");
            return false;
        } else {
            if (info.isConnected()) {
                Log.d(TAG, "Connectivity Exists !");
                return true;
            } else if (info.isAvailable()) {
                Log.d(TAG, "Network Connectivity is possible");
                return true;
            } else {
                return false;
            }

        }
    }

    public static String getCurrentTime() {
        return new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).format(new Date());
    }

    public static String getCurrentDate() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());

    }

    /*pattern:"yyyy-MM-dd HH:mm:ss"*/
    public static String getCurrentDateTime(String pattern) {
        return new SimpleDateFormat(pattern, Locale.ENGLISH).format(new Date());
    }

    public static boolean isLocationEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);


        boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return gpsStatus;
    }

    public static double calculateDistance(double startLatitude, double startLongitude, double endLatitude, double endLongitude) {
        float[] results = new float[3];
        Location.distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude, results);
        return results[0];
    }

    public static Location getCurrentLocation(Context mContext) {

        String latitude = MFunction.getMyPrefVal("latitude", mContext);
        String longitude = MFunction.getMyPrefVal("longitude", mContext);
        Location currentLocation = new Location("");
        if (TextUtils.isEmpty(latitude) || TextUtils.isEmpty(longitude)) return null;

        double lat, longi;

        try {
            lat = Double.parseDouble(latitude);
            longi = Double.parseDouble(longitude);
        } catch (Exception ex) {
            lat = 0;
            longi = 0;
        }

        if (lat == 0 && longi == 0) return null;

        currentLocation.setLatitude(lat);
        currentLocation.setLongitude(longi);
        return currentLocation;
    }



    public static boolean hasLocationPermission(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
        else
            return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void showEnableLocationPermission(Context mContext) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setTitle(mContext.getString(R.string.app_name));
        alertDialog.setMessage("Please allow permissions for accessing location in the background");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        Intent i = new Intent();
                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:" + mContext.getPackageName()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        mContext.startActivity(i);

                    }
                });
        alertDialog.show();
    }


    /*Get preference values*/
    public static String getMyPrefVal(String key, Context context) {

        if (TextUtils.isEmpty(key) || null == context) return null;
        SecurePreferences preferences = new SecurePreferences(context, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        return preferences.getString(key);
    }

    /*Set preference values*/
    public static void putMyPrefVal(String key, String val, Context context) {
        SecurePreferences preferences = new SecurePreferences(context, MConstant.PREFERENCE_NAME, MConstant.PREFERENCE_ENCRYPT_KEY, true);
        preferences.put(key, val);
    }


    public static void logToast(Context context,String data) {
        if (!MConstant.DEBUG) return;
        Toast.makeText(context, data, Toast.LENGTH_LONG).show();

    }

    public static String getDate(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        //Calendar calendar = Calendar.getInstance();
        //calendar.setTimeInMillis(milliSeconds);
        return formatter.format(milliSeconds);
    }

    public static long getUnixTimestamp(String datetime) {

        if (TextUtils.isEmpty(datetime)) return 0;

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        if (datetime.trim().length() < 19) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        }

        Date date = null;
        try {
            date = dateFormat.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static long getUnixTimestamp(String datetime, String pattern) {
        if (TextUtils.isEmpty(datetime) || TextUtils.isEmpty(pattern)) return 0;

        DateFormat dateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);
        Date date = null;
        try {
            date = dateFormat.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long time = date == null ? null : date.getTime();
        Log.d(TAG, "getUnixTimestamp: time=" + time);
        return time;
    }

    public static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

  /*  public static String getPlaceName(String latitude, String longitude,Context context){
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        String address = "";
        try {

            addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();


        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }*/


}
