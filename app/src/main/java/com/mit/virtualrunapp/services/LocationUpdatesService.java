

package com.mit.virtualrunapp.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.RingtoneManager;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.mit.virtualrunapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.mit.virtualrunapp.admindashboard.MainActivity;
import com.mit.virtualrunapp.helper.UserRunTracker;
import com.mit.virtualrunapp.utils.MFunction;
import com.mit.virtualrunapp.utils.UserSessionManager;
import com.mit.virtualrunapp.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;
import java.util.Random;


public class LocationUpdatesService extends Service {

    int checkout_status;

    private static final String PACKAGE_NAME =
            "com.google.android.gms.location.sample.locationupdatesforegroundservice";

    private static final String TAG = LocationUpdatesService.class.getSimpleName();

    /**
     * The name of the channel for notifications.
     */
    private static final String CHANNEL_ID = "channel_01";

    public static final String ACTION_BROADCAST = PACKAGE_NAME + ".broadcast";

    public static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

    private final IBinder mBinder = new LocalBinder();

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 15000;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 12345678;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;

    /**
     * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
     */
    private LocationRequest mLocationRequest;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Callback for changes in location.
     */
    private LocationCallback mLocationCallback;

    private Handler mServiceHandler;

    /**
     * The current location.
     */
    private Location mLocation;

    private float mAccuracy;
    private String mActivity;
    Boolean checkinClicked = false;

    public static final String KEY_LOCATION_DISTANCE_FACTOR = "location_distance_factor";
    public static final String KEY_LOCATION_FETCH_INTERVAL = "location_fetch_interval";
    public static final String KEY_LOCATION_ACCEPTABLE_ACCURACY = "location_acceptable_accuracy";

  //  private FirebaseRemoteConfig remoteConfig;

    Location forCheckOut = null;


    public LocationUpdatesService() {
    }

    @Override
    public void onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        checkout_status = 0;
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
               // saveLocationTrigger(locationAvailability.isLocationAvailable() ? 1 : 0);
                // TODO saveLocation  saveLocationTrigger(locationAvailability.isLocationAvailable() ? 1 : 0);
            }

        };

        createLocationRequest();
        getLastLocation();

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }



    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service started");
        boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
                false);
        checkout_status = 0;

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates();
            stopSelf();
        }

        if (null != intent) {
            checkinClicked = intent.getBooleanExtra("checkin_clicked", false);
        }

        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mChangingConfiguration = true;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
      //  MFunction.logToast("Low Memory");
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    //    MFunction.logToast("onTrimMemory level:" + level);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.e(TAG, "in onBind()");
        stopForeground(true);
        mChangingConfiguration = false;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.e(TAG, "in onRebind()");
        stopForeground(true);
        mChangingConfiguration = false;
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Last client unbound from service");

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && Utils.requestingLocationUpdates(this)) {
            Log.e(TAG, "Starting foreground service");

            startForeground(NOTIFICATION_ID, getNotification());
        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        mServiceHandler.removeCallbacksAndMessages(null);
        if (forCheckOut != null) {
            onNewLocation(forCheckOut);
            checkout_status = 1;
        }
        removeLocationUpdates();

    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates");
        checkout_status = 0;
        Utils.setRequestingLocationUpdates(this, true);
       /* Intent intent = new Intent(getApplicationContext(), LocationUpdatesService.class);
        boolean clicked = Utils.checkinClicked(getApplicationContext());
        intent.putExtra("checkin_clicked", clicked);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getApplicationContext().startForegroundService(intent);
        } else {
            startService(intent);
        }*/
        startService(new Intent(getApplicationContext(), LocationUpdatesService.class));
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, false);
            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        Log.i(TAG, "Removing location updates");
        if (forCheckOut != null) {
            onNewLocation(forCheckOut);

        }
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            Utils.setRequestingLocationUpdates(this, false);
            stopSelf();
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, true);
            Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, LocationUpdatesService.class);

        CharSequence text = Utils.getLocationText(mLocation);

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        //  intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        //    PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                //  .addAction(R.drawable.icon, getString(R.string.app_name),activityPendingIntent)
                //  .addAction(R.drawable.ic_cancel, getString(R.string.remove_location_updates),servicePendingIntent)
                // .setContentText(text)
                .setContentTitle("Virtual Run App")
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(activityPendingIntent)
                .setTicker("")
                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        return builder.build();
    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                mLocation = task.getResult();
                            } else {
                                Log.w(TAG, "Failed to get location.");
                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }
    }

    private void onNewLocation(Location location) {
        Log.e("checkoutStat", checkout_status + "");
        if (checkout_status == 0) {
            Log.e("checkoutStat", checkout_status + "");
            String distance = "0";
            float speed = 0;

            Log.i(TAG, "New location: " + location);
            mLocation = location;
            /*Sanjay Karki Monitoring Battery Level*/
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = this.registerReceiver(null, ifilter);
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            float batteryPct = level * 100 / (float) scale;
            /* End Of Battery Monitoring*/


            if(new UserSessionManager(getApplicationContext()).getPreviousLat()!=null){
              distance =""+  MFunction.calculateDistance(Double.valueOf(new UserSessionManager(getApplicationContext()).getPreviousLat()),Double.valueOf(new UserSessionManager(getApplicationContext()).getPreviousLong()),location.getLatitude(),location.getLongitude());
                speed = Float.parseFloat(distance)/((MFunction.getUnixTimestamp(MFunction.getCurrentDateTime("yyyy-MM-dd HH:mm:ss"))-MFunction.getUnixTimestamp(new UserSessionManager(getApplicationContext()).getUserTimeLastLocationTrack()))/1000);
            }

            Log.i("checkingLatLongDistance",distance);

            Log.i("checkingLatLongSpeed",speed+"");

            new UserSessionManager(getApplicationContext()).setUserTimeLastLocationTrack(MFunction.getCurrentDateTime("yyyy-MM-dd HH:mm:ss"));





            new UserSessionManager(getApplicationContext()).setUserPreviouseLat(String.valueOf(location.getLatitude()));
            new UserSessionManager(getApplicationContext()).setUserPreviouseLong(String.valueOf(location.getLongitude()));

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            String address = "";

            DatabaseReference reference;

            Double distcheck = Double.valueOf(distance);

            if(distcheck<30){
                reference = FirebaseDatabase.getInstance().getReference("eventruntracker");
                UserRunTracker userRunTracker = new UserRunTracker(location.getLatitude()+"",location.getLongitude()+"",speed+"",distance,new UserSessionManager(getApplicationContext()).getUserRunningEvent(),new UserSessionManager(getApplicationContext()).getUserName());
                Toast.makeText(getApplicationContext(),"Your data sent "+" Lat: " +location.getLatitude()+ " Long: "+location.getLongitude()+" Distance: "+distance,Toast.LENGTH_LONG).show();

                reference.child(new UserSessionManager(getApplicationContext()).getUserRunningEvent()+""+new UserSessionManager(getApplicationContext()).getUserName()).child(MFunction.getCurrentDateTime("yyyy-MM-dd HH:mm:ss")).setValue(userRunTracker, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable @org.jetbrains.annotations.Nullable DatabaseError error, @NonNull @NotNull DatabaseReference ref) {

                    }
                });
            } else {
                Toast.makeText(getApplicationContext(),"Distance cover is above normal person.",Toast.LENGTH_LONG).show();
            }

            Intent intent = new Intent(ACTION_BROADCAST);
            intent.putExtra(EXTRA_LOCATION, location);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

              // Update notification content if running as a foreground service.
            if (serviceIsRunningInForeground(this)) {
                mNotificationManager.notify(NOTIFICATION_ID, getNotification());
            }
        }
    }

    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
     //   remoteConfig = MyApp.getFirebaseRemoteConfig();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public LocationUpdatesService getService() {
            return LocationUpdatesService.this;
        }
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }




    private void showGPSInfoNotification(int locationAvailable) {

        if (locationAvailable == 1) {
            NotificationManagerCompat.from(getApplicationContext()).cancel(666444);
            return;
        }

        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addNextIntentWithParentStack(i);

        //@param1 used random for first param for multiple notifications of same time
        PendingIntent contentIntent = stackBuilder.getPendingIntent(new Random().nextInt(), PendingIntent.FLAG_UPDATE_CURRENT);

        //PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "GPS", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription("GPS watcher");
            getApplicationContext().getSystemService(NotificationManager.class).createNotificationChannel(channel);
        }

        Notification notif = new NotificationCompat.Builder(getApplicationContext(), TAG)
                .setContentTitle(getString(R.string.text_location_disabled))
                .setContentText(getString(R.string.text_location_inaccurate_turn_gps))
                .setAutoCancel(false)
                .setChannelId(CHANNEL_ID)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.virtual_run_app_logo)
                .setShowWhen(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.text_location_inaccurate_turn_gps)))
                .setColor(Color.GREEN)
                .setLocalOnly(true)
                .build();
        notif.flags |= Notification.FLAG_ONGOING_EVENT;
        NotificationManagerCompat.from(getApplicationContext()).notify(666444, notif);
    }

}
