package com.mit.virtualrunapp.helper;

public class EventTakerHelper {
    String eventname  , status , username;

    public EventTakerHelper(String eventname, String status, String username){
        this.eventname = eventname;
        this.status = status;
        this.username = username;
    }

    public EventTakerHelper(){

    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
