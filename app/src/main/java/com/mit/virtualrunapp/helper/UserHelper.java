package com.mit.virtualrunapp.helper;

public class UserHelper {
    String username, email, password, phone, mode, status,address;

    public UserHelper(String username, String email, String password,String phone, String mode,String status,String address){
        this.username = username;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.mode = mode;
        this.status =status;
        this.address = address;
    }

    public UserHelper(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
